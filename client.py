#!/usr/bin/python
###################################################################
# Copyright (C) BILA
###################################################################

import os, sys, time
from optparse import OptionParser
import string, re
import socket

def parse_args(args):
        """Parse arguments"""

        usage = """client.py """
        parser = OptionParser(version = "client.py 0.1", usage=usage)
        # query options
        parser.add_option("-v", "--verbose", default=False,
                        action="store_true", help="Verbose")
        parser.add_option("-d", "--debug", default=False,
                        action="store_true", help="only print what would have been sent")
        parser.add_option("-p", "--punctuation", default=False,
                        action="store_true", help="sentences must end in punctuation - otherwise treats each line as an independant input")
        parser.add_option("-s", "--script", default=None,
                        help="script to process")
        parser.add_option("-r", "--role", default=None,
                        help="role to process from script")
        parser.add_option("-n", "--notrole", default=None,
                        help="process all other roles from script but this")
        parser.add_option("-t", "--targetport", default="1337",
                        help="port where the server is listening (default 1337)")
        (opts, argsleft) = parser.parse_args(args)

        return opts

def main(args):
	"""main"""

	opts = parse_args(args)

	script = "testscript.txt"
	end_in_punctuation = False
	if opts.script:
		script = opts.script
	role = ""
	notrole = ""
	if opts.role:
		role = opts.role
	elif opts.notrole:
		notrole = opts.notrole
	if opts.punctuation:
		end_in_punctuation = opts.punctuation

	if not os.path.exists(script):
		print "Script file %s does not exist." % (script)
		return

	try:
		fp = open(script, "r")
	except:
		print "Error opening script %s for reading." % (script)
		return

	debug_only = False
	if opts.debug:
		debug_only = True

	sock = None
	if not debug_only:
		sock = socket.socket()
		sock.connect(("127.0.0.1", string.atoi(opts.targetport)))
		sock.setblocking(0)
		print "Resetting state..."
		sock.sendall("forget everything silently\n")
		time.sleep(1.0)
		idata = ""
		try:
			idata = sock.recv(1024)
		except:
			pass
		if idata != "":
			idata = idata.strip()
			print "Initial recvdata: ("+ idata +")"
	line = ""
	lrole = ""
	l = fp.readline()
	while l != "":
		l = re.sub(" +\n", "\n", l)
		#print "Read ("+ l +")"
		oldline = line
		rolechange = False
		if lrole != "" and (re.search(": ", l) or re.match("^Q\. .*", l) or re.match("^A\. .*", l)):
			rolechange = True
		else:
			line += " "+ l
		l = l.strip()
		#print "Line ("+ line +")"
		if rolechange or l == "" or (end_in_punctuation and re.search("[\.\?!]\n?$", line)) or (not end_in_punctuation and re.search("\n$", line)):
			#print "end of text block"
			#if rolechange:
			#	print "role change from old role ("+ lrole +")."
			line = re.sub("\n", " ", line)
			line = line.strip()
			# Figure out the role for this line.
			if re.search(": ", line):
				lrole = re.sub("^([^:]+): .*$", "\\1", line)
				line = re.sub("^([^:]+): ", "", line)
				#print "setting lrole ("+ lrole +")"
			elif re.match("^Q\.$", line):
				lrole = "Q"
				line = ""
			elif re.match("^Q\. .*", line):
				lrole = "Q"
				line = re.sub("^Q\. ", "", line)
				#print "setting lrole ("+ lrole +")"
			elif re.match("^A\. .*", line):
				lrole = "A"
				line = re.sub("^A\. ", "", line)
				#print "setting lrole ("+ lrole +")"
			# See if there is a role change in the new fraction
			newrole = ""
			if re.search(": ", l):
				newrole = re.sub("^([^:]+): .*$", "\\1", l)
				l = re.sub("^([^:]+): ", "", l)
				if rolechange:
					l = newrole +": "+ l
			elif re.match("^Q\. .*", l):
				newrole = "Q"
				l = re.sub("^Q\. ", "", l)
				if rolechange:
					l = newrole +": "+ l
			elif re.match("^A\. .*", l):
				newrole = "A"
				l = re.sub("^A\. ", "", l)
				if rolechange:
					l = newrole +": "+ l
			# If there's a role change, set line back a little
			if newrole != "":
				oldline = re.sub("\n", " ", oldline)
				line = oldline.strip()
				#print "Found new role ("+ newrole +") processing old line ("+ line +") for old role ("+ lrole +")"
			line = re.sub(" +", " ", line)
			if line == "":
				if newrole != "":
					line = l
					#print "Line for newrole ("+ newrole +") is ("+ line +")"
				l = fp.readline()
				continue
			if (role == "" and notrole == "") or (role != "" and role == lrole) or (notrole != "" and notrole != lrole):
				if re.match("^\".*", line):
					line = re.sub("^\"", "", line)
					line = re.sub("\"$", "", line)
				if lrole != "":
					print lrole +"> " + line
					if not debug_only:
						sock.sendall(line +"\n")
					time.sleep(1.0)
				else:
					print "Send> " + line
					if not debug_only:
						sock.sendall(line +"\n")
					time.sleep(1.0)
			# lrole only changes when we think there's
			# a newly declared role, but line changes each time
			# through this section
			if newrole != "":
				line = l
				#print "Line for newrole ("+ newrole +") is ("+ line +")"
			else:
				line = ""
		if not debug_only:
			idata = ""
			try:
				idata = sock.recv(1024)
			except:
				pass
			if idata != "":
				idata = idata.strip()
				prefix = ""
				if re.match(".*Saving empty response state for:.*", idata):
					prefix = "** "
				print prefix + "Recv: ("+ idata +")\n"
		l = fp.readline()
	print "done"
	fp.close()
	# See if there's anything left to send
	if line != "":
		# Figure out the role for this line.
		if re.search(": ", line):
			lrole = re.sub("^([^:]+): .*$", "\\1", line)
			line = re.sub("^([^:]+): ", "", line)
		elif re.match("^Q\. .*", line):
			lrole = "Q"
			line = re.sub("^Q\. ", "", line)
		elif re.match("^A\. .*", line):
			lrole = "A"
			line = re.sub("^A\. ", "", line)
	if line != "" and ((role == "" and notrole == "") or (role != "" and role == lrole) or (notrole != "" and notrole != lrole)):
		if lrole != "":
			print lrole +"> " + line
			if not debug_only:
				sock.sendall(line +"\n")
			time.sleep(1.0)
		else:
			print "Send> " + line
			if not debug_only:
				sock.sendall(line +"\n")
			time.sleep(1.0)
		if not debug_only:
			idata = ""
			try:
				idata = sock.recv(1024)
			except:
				pass
			if idata != "":
				idata = idata.strip()
				prefix = ""
				if re.match(".*Saving empty response state for:.*", idata):
					prefix = "** "
				print prefix + "Recv: ("+ idata +")\n"
	if not debug_only:
		sock.close()

if __name__ == "__main__":
        main(sys.argv[1:])

