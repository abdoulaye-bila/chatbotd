#!/usr/bin/python
###################################################################
# Copyright (C) BILA

import os, sys, time
import subprocess
from threading import Thread
from optparse import OptionParser
import string
import re
import random
from copy import deepcopy
import socket
userhome = os.path.expanduser("~")

# PyAIMLng and SnakeSQL are required
# aimlGOAP and PyGOAPng are optional
sys.path.append(userhome +"/PyAIMLng-0.8.5")
sys.path.append(userhome +"/SnakeSQL")
sys.path.append(userhome +"/aimlGOAP-0.1.0")
sys.path.append(userhome +"/PyGOAPng-0.3.1")
import aiml
import SnakeSQL

angie_version = "0.5.0"
eThread = None
iThread = None
vThread = None
sThread = None
gThread = None
has_goap = False
periodicCommands = []
taskCommands = []
live_aiml_filename = "live-templates.aiml"
live_gossip_filename = "live-gossip.aiml"
command_path = userhome +"/angie-"+ angie_version +"/bin"
aimlgoap_bot_path = userhome +"/aimlGOAP-0.1.0/aimlgoap/bot"

# aimlgoap relies on pygoapng.  If it's not installed, don't use GOAP features.
try:
	import pygoapng
	has_goap = True
except:
	has_goap = False
	pass

class executionSubThread(Thread):
	def __init__(self, exec_args, isAIML = False, header = "", footer = ""):
		Thread.__init__(self)
		self.name = "executionSubThread"
		self.cmd = exec_args[0]
		self.exec_args = exec_args
		self.stop_requested = False
		self.isAIML = isAIML
		self.header = header
		self.footer = footer
		self.done = False
		self.remove = False
		self.vocalize = False
		self.socket = None
		self.results = ""
	def run(self):
		sub_process = subprocess.Popen(self.exec_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		if sub_process.wait() != 0:
			print "executionSubThread:: "+ self.name +":: error executing sub process: "+ string.join(self.exec_args, " ")
		(stdout_data, stderr_data) = sub_process.communicate()
		if self.header != "" or self.footer != "":
			# Header and footer are added by executionThread.run()
			self.results = stdout_data
		else:
			self.results = self.cmd +" results: "+ stdout_data
		self.done = True
	def setVocalize(self, value = False):
		self.vocalize = value
	def setSocket(self, value = None):
		self.socket = value

class executionThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.name = "executionThread"
		self.stop_requested = False
		self.results_queue = []
		self.vocalize_queue = []
		self.socket_queue = []
		self.aiml_queue = []
		self.aiml_vocalize_queue = []
		self.aiml_socket_queue = []
		self.subthreads = []
	def run(self):
		# Wait for commands to process in sub-threads
		# and put results in a result queue for main to
		# poll
		while not self.stop_requested:
			time.sleep(0.1)
			i = 0
			while i < len(self.subthreads):
				if self.subthreads[i].done:
					#print "subthread %d is done." % (i)
					if self.subthreads[i].isAIML:
						self.aiml_queue.append(self.subthreads[i].header + self.subthreads[i].results + self.subthreads[i].footer)
						self.aiml_vocalize_queue.append(self.subthreads[i].vocalize)
						self.aiml_socket_queue.append(self.subthreads[i].socket)
					else:
						self.results_queue.append(self.subthreads[i].results)
						self.vocalize_queue.append(self.subthreads[i].vocalize)
						self.socket_queue.append(self.subthreads[i].socket)
					self.subthreads[i].remove = True
				i += 1
			i = len(self.subthreads) - 1
			while i >= 0:
				if self.subthreads[i].remove:
					#print "removing subthread %d." % (i)
					self.subthreads.pop(i)
				i -= 1
	def stop(self):
		self.stop_requested = True
	def execute(self, cmd, cmd_args, vocalize = False, socket = None):
		print "exec(%s, %s)" % (cmd, string.join(cmd_args, ", "))
		exec_args = [cmd]
		for arg in cmd_args:
			exec_args.append(arg)
		subthread = executionSubThread(exec_args)
		if vocalize:
			subthread.setVocalize(True)
		subthread.setSocket(socket)
		subthread.start()
		self.subthreads.append(subthread)
	def get_next_result(self):
		retval = ["", False]
		if len(self.results_queue) > 0:
			retval = [self.results_queue.pop(0), self.vocalize_queue.pop(0), self.socket_queue.pop(0)]
		return retval
	def has_results(self):
		return self.results_ready()
	def results_ready(self):
		if len(self.results_queue) > 0:
			return True
		return False
	def executeAIMLTask(self, task, task_args = [], vocalize = False, socket = None):
		cmd = task["command"]
		cmd_args = task["command_args"]
		header = task["aiml_header"]
		footer = task["aiml_footer"]
		print "executeAIMLTask(%s, %s, %s)" % (cmd, string.join(cmd_args, ", "), string.join(task_args, ", "))
		exec_args = [cmd]
		for arg in cmd_args:
			exec_args.append(arg)
		for task_arg in task_args:
			exec_args.append(task_arg)
		subthread = executionSubThread(exec_args, True, header, footer)
		if vocalize == True:
			subthread.setVocalize(True)
		subthread.setSocket(socket)
		subthread.start()
		self.subthreads.append(subthread)
	def get_next_aiml(self):
		retval = ["", False]
		if len(self.aiml_queue) > 0:
			retval = [self.aiml_queue.pop(0), self.aiml_vocalize_queue.pop(0), self.aiml_socket_queue.pop(0)]
		return retval
	def has_aiml_results(self):
		return self.aiml_ready()
	def aiml_ready(self):
		if len(self.aiml_queue) > 0:
			return True
		return False

class socketThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.port = 1337
		self.socket = socket.socket()
		self.clientsocket = None
		self.clientaddress = ""
		self.stop_requested = False
		self.listening = False
	def run(self):
		global iThread
		while self.listening == False and not self.stop_requested:
			try:
				self.socket.bind(("127.0.0.1", self.port))
				self.listening = True
			except:
				time.sleep(1.0)
		if self.listening == True:
			self.socket.listen(5)
			self.socket.settimeout(0.5)
		while not self.stop_requested:
			print "Waiting for client socket."
			self.clientsocket = None
			while not self.stop_requested:
				time.sleep(0.10)
				try:
					(self.clientsocket, self.clientaddress) = self.socket.accept()
				except:
					continue
				if self.clientsocket is not None:
					break
			if self.stop_requested:
				break
			print "Accepted new clientsocket."
			try:
				self.clientsocket.setblocking(1)
			except:
				break
			data = ""
			d = ""
			try:
				d = self.clientsocket.recv(1024)
			except:
				break
			while d != "":
				data += d
				if re.search(".*\n$", data):
					data = data.strip()
					print "Processing socket text: ("+ data +")"
					if data == "quit":
						break
					iThread.process_input_and_output_with_mood(data, sb = "__main", socket = self.clientsocket)
					data = ""
				try:
					d = self.clientsocket.recv(1024)
				except:
					break
			print "Closing clientsocket due to 'quit' (or socket close)."
			try:
				self.clientsocket.close()
			except:
				pass
		self.quit()
	def stop(self):
		self.stop_requested = True
	def setport(self, port):
		self.port = port
	def quit(self):
		if self.clientsocket is not None:
			self.clientsocket.close()
		self.socket.close()

class interactiveThread(Thread):
	def __init__(self, k):
		Thread.__init__(self)
		self.brains = {}
		self.brains["__main"] = k
		self.brains["__main"].verbose(False)
		self.name = "interactiveThread"
		self.useHLPCtrees = False
		self.startup_mode = True
		self.stop_requested = False
		self.async_response_queue = []
		self.async_vocalize_queue = []
		self.async_socket_queue = []
		self.aiml_queue = []
		self.aiml_vocalize_queue = []
		self.aiml_socket_queue = []
		self.moods = {}
		self.mood_threshold = 10
		self.mood_decay_value = 0.1
		self.current_mood = "__main"
		self.vocalize = False
		self.showSrais = False
		self.showMatches = False
		self.debugResponses = False
		self.temp_vocalize = False
		self.useRepetitionDetector = True
		self.empty_response_filename = "empty.responses.txt"
		self.bad_response_filename = "bad.responses.txt"
		self.botvalues = {}
		self.botvalues["name"] = "Alice"
		#self.botvalues["firstname"] = "Alice"
		#self.botvalues["middlename"] = "Marie"
		#self.botvalues["lastname"] = "Rodriguez"
		self.botvalues["gender"] = "female"
		self.botvalues["hair_length"] = "long"
		self.botvalues["hair_straightness"] = "wavy"
		self.botvalues["dress_size"] = "8"
		self.botvalues["shoe_size"] = "8"
		self.botvalues["master"] = "Mud"
		self.botvalues["master_gender"] = "male"
		self.botvalues["location"] = "New York"
		self.botvalues["birthplace"] = "Dallas"
		self.botvalues["birthday"] = "July 27"
		self.botvalues["birthyear"] = "2009"
		self.normalvalues = {}
		self.loadBotDefaults()
		#self.last_seen = {}
		self.templatemode = False
		self.drafttemplate = {}
		self.drafttemplate["pattern"] = ""
		self.drafttemplate["template"] = []
		self.live_templates = {}
		self.dynpatterns = {}
		self.gossip_file = ""
		self.dbh = None
		self.graphviz_file = ""
		self.reset_path_on_load = True
		self.loi = "neutral"
		self.loi_history = ["neutral"]
		self.loi_history_max_length = 10
		self.num_sequential_goap_questions = 0
		self.max_sequential_goap_questions = 2
		self.profile_data = {'num_patterns': 0, 'num_edges': 0, 'num_srais': 0, 'num_catchalls': 0, 'num_goap_responses': 0}
	def run(self):
		global eThread, vThread
		# Loop, reading user input from the command
		# line and printing responses.
		sb = "__main"
		self.processBotValues(sb)
		self.processNormalValues(sb)
		# wait until we're told to start processing input
		while not self.stop_requested and self.startup_mode:
			time.sleep(0.1)
		if self.graphviz_file != "":
			# initialize the file and turn on track_match_path
			try:
				fp = open(self.graphviz_file, "w")
				fp.close()
				for tsb in self.brains:
					if self.reset_path_on_load:
						self.brains[tsb].resetMatchPath()
					self.brains[tsb].track_match_path = True
			except Exception, msg:
				print "Error creating graphviz file %s (%s)" % (self.graphviz_file, msg)
				self.graphviz_file = ""
				for tsb in self.brains:
					self.brains[tsb].track_match_path = False
		while not self.stop_requested:
			input = string.strip(raw_input("> "))
			if input == "":
				continue
			self.process_input_and_output_with_mood(input, sb = sb)
		if len(self.live_templates) > 0:
			print "saving live templates."
			self.saveLiveTemplates()
		if self.gossip_file != "":
			didsave = False
			for tsb in self.brains:
				if self.brains[tsb].saveGossip():
					didsave = True
			if didsave:
				print "saved new gossip templates."
		self.writeMatchPath(sb)
		print "interactive thread finished."

    	def loadBotDefaults(self):
		if os.path.exists("angie.bot.values.txt"):
			self.loadBotValuesFromFile("angie.bot.values.txt")
		self.setBotName(self.botvalues["name"], "__main")
		self.setBotMaster(self.botvalues["master"])
		if self.botvalues.has_key("firstname") and self.botvalues.has_key("middlename") and self.botvalues.has_key("lastname"):
			self.setBotFullName(self.botvalues["firstname"], self.botvalues["middlename"], self.botvalues["lastname"])
		if self.botvalues.has_key("nickname"):
			self.setBotNickName(self.botvalues["nickname"])
		self.processBotValues("__main")
		self.processNormalValues("__main")

	def loadBotValuesFromFile(self, botvalue_filename):
		if not os.path.exists(botvalue_filename):
			return 1
		try:
                	fp = open(botvalue_filename, "r")
        	except:
                	print "Error opening botvalue file %s for reading." % (botvalue_filename)
                	return 1
	
        	input = fp.readline()
        	while input != "":
			# line format is "bot_variable_name: bot_variable_value"
			# comments are allowed so long as the first character of the comment line is a "#"
                	input = input.strip()
			if not re.search("^#", input):
				botvar_name = re.sub("^([^:]+):.*", "\\1", input)
                		botvar_value = re.sub("^[^:]+:", "", input).strip()
				self.botvalues[botvar_name] = botvar_value
        		input = fp.readline()
        	fp.close()
	
		return 0

	def makeNodeLabel(self, text):
		nodelabel = re.sub("\\\\d","\\\\\\\\d", re.sub("\n","\\\\n",re.sub("\"","\\\"",re.sub("^([^:]+):(.*)$", "\\1\\\\n\\2", text))))
		return nodelabel

	def makeEdgeLabel(self, text):
		edgelabel = re.sub("\n","\\\\n",re.sub("\"","\\\"", text))
		return edgelabel

	def writeMatchPath(self, sb = "__main"):
		if self.graphviz_file == "":
			return -1
		if len(self.brains[sb].match_path) <= 0:
			return 0
		print "writeMatchPath("+ sb +")"
		fnum = 0
		max_nodes_per_file = 300
		if max_nodes_per_file < len(self.brains[sb].match_path) and len(self.brains[sb].match_path) < max_nodes_per_file * 2:
			max_nodes_per_file = len(self.brains[sb].match_path)/2
		print "writeMatchPath len(match_path) = %d, putting %d nodes in each dot file" % (len(self.brains[sb].match_path), max_nodes_per_file)
		last_node = -1
		last_think_node = -1
		line_tag = ""
		think_line_tag = ""
		shape = "box"
		thinkshape = "oval"
		inputcolor = "blue"
		matchcolor = "red"
		responsecolor = "red"
		sraicolor = "orange"
		conversationcolor = "brown"
		current_file_name = ""
		try:
			i = 0
			while i < len(self.brains[sb].match_path):
				# see if we need to create more files
				if i % max_nodes_per_file == 0:
					if i > 0:
						fp.write("}\n")
						fp.close()
						print "writeMatchPath: wrote file (%s)" % (current_file_name)
					fnum = fnum + 1
					current_file_name = re.sub("\.dot$",".%03d.dot" % (fnum), self.graphviz_file)
					print "writeMatchPath: populating file (%s)" % (current_file_name)
					fp = open(current_file_name, "w")
					fp.write("digraph g {\n")
					fp.write("graph [ ];\n")
					fp.write("node [ shape = \"%s\" ];\n" % (shape))
					fp.write("edge [ ];\n")
				if re.search("^input:", self.brains[sb].match_path[i]):
					fp.write("//node: [ "+ self.makeNodeLabel(self.brains[sb].match_path[i]) +"];\n")
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (i, inputcolor, nodelabel))
					if last_node > -1:
						if line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_node, i))
					line_tag = ""
					last_node = i
				elif re.search("^subbedinput:", self.brains[sb].match_path[i]):
					line_tag = self.brains[sb].match_path[i]
				elif re.search("^think subbedinput:", self.brains[sb].match_path[i]):
					think_line_tag = self.brains[sb].match_path[i]
				elif re.search("^match:", self.brains[sb].match_path[i]):
					fp.write("//node: [ "+ self.makeNodeLabel(self.brains[sb].match_path[i]) +"];\n")
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (i, matchcolor, nodelabel))
					if last_node > -1:
						if line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_node, i))
					line_tag = ""
					last_node = i
					last_think_node = -1
				elif re.search("^srai:", self.brains[sb].match_path[i]):
					fp.write("//node: [ "+ self.makeNodeLabel(self.brains[sb].match_path[i]) +"];\n")
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (i, sraicolor, nodelabel))
					if last_node > -1:
						if line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_node, i))
					line_tag = ""
					last_node = i
					last_think_node = -1
				elif re.search("^response:", self.brains[sb].match_path[i]):
					line_tag = self.brains[sb].match_path[i]
				elif re.search("^think srai:", self.brains[sb].match_path[i]):
					fp.write("//node: [ type=\"think\", "+ self.makeNodeLabel(self.brains[sb].match_path[i]) +"];\n")
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ shape = \"%s\", label = \"think::\\n%s\"];\n" % (i, thinkshape, nodelabel))
					if last_node > -1 and last_think_node == -1:
						if think_line_tag != "":
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(think_line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ label = \"%s\"];\n" % (last_node, i, self.makeEdgeLabel(think_line_tag)))
						elif line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_node, i))
					elif last_think_node > -1:
						if think_line_tag != "":
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(think_line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ label = \"%s\"];\n" % (last_think_node, i, self.makeEdgeLabel(think_line_tag)))
						elif line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_think_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_think_node, i))
					line_tag = ""
					think_line_tag = ""
					last_think_node = i
				elif re.search("^think match:", self.brains[sb].match_path[i]):
					fp.write("//node: [ type=\"think\", "+ self.makeNodeLabel(self.brains[sb].match_path[i]) +"];\n")
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ shape = \"%s\", label = \"think::\\n%s\"];\n" % (i, thinkshape, nodelabel))
					if last_node > -1 and last_think_node == -1:
						if think_line_tag != "":
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(think_line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ label = \"%s\"];\n" % (last_node, i, self.makeEdgeLabel(think_line_tag)))
							think_line_tag = ""
						elif line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
							line_tag = ""
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_node, i))
					elif last_think_node > -1:
						if think_line_tag != "":
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(think_line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ label = \"%s\"];\n" % (last_think_node, i, self.makeEdgeLabel(think_line_tag)))
							think_line_tag = ""
						elif line_tag != "":
							edgecolor = "black"
							if re.search("^input", line_tag):
								edgecolor = inputcolor
							elif re.search("^subbedinput", line_tag):
								edgecolor = inputcolor
								if re.search("^srai", self.brains[sb].match_path[last_node]):
									edgecolor = sraicolor
							elif re.search("^match", line_tag):
								edgecolor = matchcolor
							elif re.search("^response", line_tag):
								edgecolor = responsecolor
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
							fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_think_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
							line_tag = ""
						else:
							fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_think_node]) +" -> "+ self.makeEdgeLabel(self.brains[sb].match_path[i]) +" [];\n")
							fp.write("\"node%d\" -> \"node%d\";\n" % (last_think_node, i))
					think_line_tag = ""
					last_think_node = i
				i = i + 1
			i = i + 1
			if line_tag != "":
				fp.write("//node: [ type=\"endnode\", end];\n")
				fp.write("\"node%d\" [ label = \"endnode:: %s\"];\n" % (i, "end"))
				edgecolor = "black"
				if re.search("^input", line_tag):
					edgecolor = inputcolor
				elif re.search("^subbedinput", line_tag):
					edgecolor = inputcolor
					if re.search("^srai", self.brains[sb].match_path[last_node]):
						edgecolor = sraicolor
				elif re.search("^match", line_tag):
					edgecolor = matchcolor
				elif re.search("^response", line_tag):
					edgecolor = responsecolor
				fp.write("//edge: "+ self.makeEdgeLabel(self.brains[sb].match_path[last_node]) +" -> end [linetag: "+ self.makeEdgeLabel(line_tag) +"];\n")
				fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (last_node, i, edgecolor, self.makeEdgeLabel(line_tag)))
				last_node = i
				line_tag = ""
			# now go back and write out the input/response chain
			idx = last_node + 1
			i = 1
			last_node = 0
			while i < len(self.brains[sb].match_path):
				if re.search("^input:", self.brains[sb].match_path[i]) or re.search("^response:", self.brains[sb].match_path[i]):
					nodelabel = self.makeNodeLabel(self.brains[sb].match_path[i])
					fp.write("\"node%d\" [ fontcolor = \"%s\", label = \"%s\"];\n" % (idx, conversationcolor, nodelabel))
					fp.write("\"node%d\" -> \"node%d\" [ fontcolor = \"%s\"];\n" % (last_node, idx, conversationcolor))
					last_node = idx
					idx = idx + 1
				i = i + 1
			fp.write("}\n")
			fp.close()
			print "writeMatchPath: wrote file (%s)" % (current_file_name)
		except Exception, msg:
			print "writeMatchPath: Error writing to graphviz file %s (%s)" % (self.graphviz_file, msg)
			return -1
		print "writeMatchPath: done"
		return 1

	def startProcessingInput(self):
		self.startup_mode = False

	def process_input_and_output_with_mood(self, input, sb = "__main", socket = None):
		new_mood = self.chooseMood()
		if new_mood != self.current_mood and self.brains.has_key(new_mood):
			msg = "Changing mood from ("+ self.current_mood +") to ("+ new_mood +")..."
			if socket is None:
				print msg
			else:
				try:
					socket.sendall(msg+"\n")
				except:
					pass
		else:
			new_mood = self.current_mood
		self.current_mood = new_mood
		# The current brain and the main need to know what
		# the mood state is before they process input.
		self.saveMoodsToBrain(sb = self.current_mood)
		if sb != "__main":
			self.saveMoodsToBrain(sb = "__main")
		resp = self.process_input(input, sb = self.current_mood, socket = socket)
		self.process_output(resp, sb = self.current_mood, socket = socket)
		self.decayMoods()
		for mood in self.moods:
			if self.debugResponses and self.moods[mood] > 0:
				msg = "Mood: "+ mood +" = "+ str(self.moods[mood])
				if socket is None:
					print msg
				else:
					try:
						socket.sendall(msg+"\n")
					except:
						pass

	def saveLiveTemplates(self):
		global live_aiml_filename
		if len(self.live_templates) <= 0:
			return
		try:
			fp = open(live_aiml_filename, "w")
			fp.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
			fp.write("<aiml version=\"1.0\">\n")
			fp.write("<meta name=\"language\" content=\"en\"/>\n")
			fp.write("\n")
			# These are the templates that have been
			# added since we began running.
			for key,tem in self.live_templates.items():
				(pattern,that,topic,restriction) = key
				(restrictedA, restrictedB, restrictedOp) = restriction
				isRestricted = False
				if restrictedA != "" or restrictedB != "" or restrictedOp != "":
					isRestricted = True
				if isRestricted:
					fp.write("<restricted a=\""+ restrictedA +"\" b=\""+ restrictedB +"\" op=\""+ restrictedOp +"\">\n")
				if topic != "" and topic != "*":
					fp.write("<topic name=\""+ topic +"\">\n")
				fp.write("<category>\n")
				fp.write("<pattern>"+ pattern +"</pattern>\n")
				if that != "" and that != "*":
					fp.write("<that>"+ that +"</that>\n")
				fp.write(re.sub("&","&amp;", self.brains["__main"]._elementToText(tem)))
				fp.write("\n")
				fp.write("</category>\n")
				if topic != "" and topic != "*":
					fp.write("</topic>\n")
				if isRestricted:
					fp.write("</restricted>\n")
				fp.write("\n")
			# These are the ones that were in the file
			# when we were first started up.
			for key,tem in self.dynpatterns.items():
				(pattern,that,topic,restriction) = key
				(restrictedA, restrictedB, restrictedOp) = restriction
				isRestricted = False
				if restrictedA != "" or restrictedB != "" or restrictedOp != "":
					isRestricted = True
				if isRestricted:
					fp.write("<restricted a=\""+ restrictedA +"\" b=\""+ restrictedB +"\" op=\""+ restrictedOp +"\">\n")
				if topic != "" and topic != "*":
					fp.write("<topic name=\""+ topic +"\">\n")
				fp.write("<category>\n")
				fp.write("<pattern>"+ pattern +"</pattern>\n")
				if that != "" and that != "*":
					fp.write("<that>"+ that +"</that>\n")
				fp.write(re.sub("&","&amp;", self.brains["__main"]._elementToText(tem)))
				fp.write("\n")
				fp.write("</category>\n")
				if topic != "" and topic != "*":
					fp.write("</topic>\n")
				if isRestricted:
					fp.write("</restricted>\n")
				fp.write("\n")

			fp.write("</aiml>\n")
			fp.close()
			print "Created live template aiml file: "+ live_aiml_filename
		except Exception, msg:
			print "Error creating live template aiml file: "+ live_aiml_filename
			print "Error is: "+ str(msg)

	def setGraphvizFile(self, gvfile):
		self.graphviz_file = gvfile
	def setDBConnection(self, _dbh):
		self.dbh = _dbh
		for sb in self.brains:
			self.brains[sb].setDBConnection(self.dbh)
	def setUseHLPCTrees(self, value):
		self.useHLPCtrees = value
	def getUseHLPCTrees(self):
		return self.useHLPCtrees
	def process_input_and_output(self, input):
		resp = self.process_input(input, sb = self.current_mood)
		self.process_output(resp, sb = self.current_mood)
	def chooseMood(self):
		next_mood = "__main"
		next_mood_value = self.mood_threshold
		for mood in self.moods:
			if self.moods[mood] > next_mood_value:
				next_mood = mood
				next_mood_value = self.moods[mood]
		return next_mood
	def cleanup_output(self, output):
		# strip multiple whitespace and whitespace before punctuation
		output = re.sub("(\s)\s+","\\1", output)
		output = re.sub("\s+([\.?!\"\'])$","\\1", output)
		return output
	def add_spelling_errors(self, output, randomize = 10, max_errors = 1):
		error_count = 0
		# contractions
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("do not",output):
			output = re.sub("do not","don't", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("I will",output):
			output = re.sub("I will","I'll", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("I would",output):
			output = re.sub("I would","I'd", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("you are",output):
			output = re.sub("you are","you're", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("we are",output):
			output = re.sub("we are","we're", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("it is",output):
			output = re.sub("it is","it's", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("she is",output):
			output = re.sub("she is","she's", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("he is",output):
			output = re.sub("he is","he's", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("it would",output):
			output = re.sub("it would","it'd", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("she would",output):
			output = re.sub("she would","she'd", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("he would",output):
			output = re.sub("he would","he'd", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("they would",output):
			output = re.sub("they would","they'd", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("it has been",output):
			output = re.sub("it has been","it's been", output, count = 1)
			error_count += 1

		# equivilent conversions
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("has to be",output):
			output = re.sub("has to be","has got to be", output, count = 1)
			error_count += 1

		# sloppy
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("I do not know",output):
			output = re.sub("I do not know","I dunno", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("want to",output):
			output = re.sub("want to","wanna", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("kind of",output):
			output = re.sub("kind of","kinda", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("going to",output):
			output = re.sub("going to","gonna", output, count = 1)
			error_count += 1

		# actual spelling errors
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("cei",output):
			output = re.sub("cei","cie", output, count = 1)
			error_count += 1
		if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("wei",output):
			output = re.sub("wei","wie", output, count = 1)
			error_count += 1
		# this changes the meaning, so don't use it
		#if error_count < max_errors and random.randint(1, 100) <= randomize and re.search("you are",output):
		#	output = re.sub("you are","your", output, count = 1)
		#	error_count += 1
		return output
	def process_output(self, output, sb = "__main", socket = None):
		####
		# This gets called after process_input() which has already 
		# taken a first pass at interpretting the response for 
		# things that the program might need to do (exec tasks and 
		# so forth).
		# This function works on output that the user will see.
		# It tracks empty responses, vocalizes responses, and 
		# adds spelling errors.
		vocalize = False
		if self.temp_vocalize or self.vocalize:
			vocalize = True
		self.temp_vocalize = False
		if output != "":
			topic = self.brains[sb].getPredicate("topic")
			prev_topic = self.brains[sb].getPredicate("prev_topic")
			prev2_topic = self.brains[sb].getPredicate("prev2_topic")
			## If socket, print to socket instead of stdout
			if self.debugResponses and topic != "":
				msg = "Topic: %s, Prev: %s, Prev2: %s" % (topic, prev_topic, prev2_topic)
				if socket is None:
					print msg
				else:
					try:
						socket.sendall(msg+"\n")
					except:
						pass
			modified_output = self.cleanup_output(output)
			modified_output = self.add_spelling_errors(modified_output)
			if socket is None:
				print modified_output
			else:
				try:
					socket.sendall(modified_output+"\n")
				except:
					pass
			if vocalize:
				# Don't vocalize task execution informational
				# messages.
				if not re.search("^Executing task: .*", output):
					vThread.say(output, False)
		elif sb != "__main":
			## subbrains get penalized for having no
			## response (in addition to normal decay cycle)...
			self.decayMood(sb)
		# Track info about empty responses, but not when we're
		# quitting or were just fed a secrettasktag
		if output == "" and not self.stop_requested and self.brains[sb].getInput(index = 1).upper() != "SECRETTASKTAG":
			msg = "Saving empty response state for: "+ self.brains[sb].getInput(index = 1)
			if self.brains[sb].lastSraiPattern != "":
				msg += "\nLast srai pattern: "+ self.brains[sb].lastSraiPattern
			if socket is None:
				print msg
			else:
				try:
					socket.sendall(msg+"\n")
				except:
					pass
			try:
				fp = open(self.empty_response_filename, "a")
				fp.write("Empty response, state was:\n")
				fp.write("Time: "+ time.strftime("%Y%m%d %H:%M:%S", time.localtime(time.time())) +"\n")
				fp.write("Input 2: "+ self.brains[sb].getInput(index = 2) +"\n")
				fp.write("That 2: "+ self.brains[sb].getThat(index = 2) +"\n")
				fp.write("Input 1: "+ self.brains[sb].getInput(index = 1) +"\n")
				fp.write("That 1: "+ self.brains[sb].getThat(index = 1) +"\n")
				if self.brains[sb].lastSraiPattern != "":
					fp.write("Last srai pattern: "+ self.brains[sb].lastSraiPattern +"\n")
				fp.write("--------------------------\n")
				fp.close()
			except:
				print "Error storing empty response to file: "+ self.empty_response_filename
	def setMoodThreshold(self, value):
		self.mood_threshold = value
	def increaseMood(self, mood, value = 1):
		if not mood in self.moods:
			self.moods[mood] = 0
		self.moods[mood] = self.moods[mood] + value
		self.setBotMoodFromMoods()
	def decreaseMood(self, mood, value = 1):
		if not mood in self.moods:
			self.moods[mood] = 0
		self.moods[mood] = self.moods[mood] - value
		if self.moods[mood] < 0:
			self.moods[mood] = 0
		self.setBotMoodFromMoods()
	def setBotMoodFromMoods(self):
		newmood = ""
		newmoodvalue = 0
		for mood in self.moods:
			if self.moods[mood] > newmoodvalue:
				newmood = mood
		if newmood != "":
			self.setValue("bot_mood", newmood)
	def getMood(self, mood):
		if not mood in self.moods:
			return 0
		return self.moods[mood]
	def saveMoodsToBrain(self, sb = "__main"):
		mood_list = []
		for mood in self.moods:
			self.setValue("bot_mood_"+mood, self.moods[mood], sb = sb)
			mood_list.append(mood)
		self.brains[sb].setPredicate("__array_bot_mood_list", mood_list)
	def decayMoods(self):
		for mood in self.moods:
			self.decayMood(mood)
	def decayMood(self, mood):
		if mood in self.moods:
			self.decreaseMood(mood, value=self.mood_decay_value)
	def setAgeFromBirthDay(self):
		self.setSignFromBirthDay()
		if self.getBotValue("birthday") == "" or self.getBotValue("birthyear") == "":
			return
		btup = time.strptime(self.getBotValue("birthday") +" "+ self.getBotValue("birthyear"),"%B %d %Y")
		try:
			years = int((time.time() - time.mktime(btup)) / (3600 * 24 * 365))
			self.setBotValue("age", str(years))
		except:
			pass
	def setSignFromBirthDay(self):
		if self.getBotValue("birthday") == "":
			return
		btup = time.strptime(self.getBotValue("birthday") +" 1970","%B %d %Y")
		month = btup.tm_mon - 1
		if btup.tm_mon == 1 and btup.tm_mday < 20:
			month = month - 1
		elif (btup.tm_mon == 6 or btup.tm_mon == 10  or btup.tm_mon == 11) and btup.tm_mday < 23:
			month = month - 1
		elif btup.tm_mon == 7 and btup.tm_mday < 22:
			month = month - 1
		elif (btup.tm_mon == 8 or btup.tm_mon == 9) and btup.tm_mday < 24:
			month = month - 1
		elif btup.tm_mday < 21:
			month = month - 1
		if month < 0:
			month = 11
		signs = ('Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn')
		if month < 12:
			self.setBotValue("sign", signs[month])
	def process_input(self, input, sb = "__main", socket = None):
		self.setAgeFromBirthDay()
		#######
		# This function processes input text (from the user or from
		# other function calls) and inspects both the input and 
		# response for instructions (set variables, exec tasks, etc.)
		# processes loi, diverts i/o to the GOAP brain, and so forth.
		input = string.strip(input)
		# Change two or two periods at the end to a single
		input = re.sub("\.\.+$",".", input)
		# And change three or more periods in the middle to a single
		input = re.sub("\.\.\.+",".", input)
		resp = ""
		loi_info = ""
		get_goap_resp = False
		################
		## Process input
		################
		if input == "quit":
			self.stop_requested = True
		elif re.search("^topic: .*", input) and self.templatemode:
			input = string.upper(re.sub("^topic: ", "", input)).strip()
			if input != "" and input != "*":
				self.drafttemplate["topic"] = input
				resp = "live template topic is: "+ self.drafttemplate["topic"]
		elif re.search("^that: .*", input) and self.templatemode:
			input = string.upper(re.sub("^that: ", "", input)).strip()
			if input != "" and input != "*":
				self.drafttemplate["that"] = input
				resp = "live template that is: "+ self.drafttemplate["that"]
		elif (re.search("^end template.*", input) or input == ".") and self.templatemode:
			self.addCategoryFromDraftTemplate(self.drafttemplate)
			self.templatemode = False
			self.drafttemplate = {}
			self.drafttemplate["pattern"] = ""
			self.drafttemplate["template"] = []
			resp = "added new live template."
		elif re.search("^delete pattern .*", input):
			username = self.getValue("name", sb = sb)
			botmaster = self.getBotValue("master", sb = sb)
			if username.lower() == botmaster.lower():
				input = string.upper(re.sub("^delete pattern ", "", input))
				input.strip()
				if input != "" and input != "*":
					self.drafttemplate = {}
					self.drafttemplate["pattern"] = input
					self.drafttemplate["meta"] = {}
					self.drafttemplate["meta"]["deleteme"] = "1"
					self.drafttemplate["template"] = []
					self.deleteCategoryFromDraftTemplate(self.drafttemplate)
					self.drafttemplate = {}
					self.drafttemplate["pattern"] = ""
					self.templatemode = False
					resp = "deleted pattern (%s)" % (input)
			else:
				resp = "you are not authorized to delete patterns"
		elif self.templatemode:
			self.drafttemplate["template"].append(input)
			# save the text as part of the new template...
			resp = "live template has "+ str(len(self.drafttemplate["template"])) +" lines"
		elif re.search("^start template .*", input) or re.search("^add template .*", input):
			username = self.getValue("name", sb = sb)
			botmaster = self.getBotValue("master", sb = sb)
			if username.lower() == botmaster.lower():
				if re.search("^start template .*", input):
					input = string.upper(re.sub("^start template ", "", input))
				else:
					input = string.upper(re.sub("^add template ", "", input))
				input.strip()
				if input != "" and input != "*":
					self.templatemode = True
					self.drafttemplate = {}
					self.drafttemplate["pattern"] = input
					self.drafttemplate["template"] = []
					resp = "enter contents of new template. start a line with \"topic: \" or \"that: \" to set <topic> or <that>. end with \"end template\" or \".\". all other lines are the response body (which can include aiml markup)."
				else:
					resp = "Usage: start template PATTERN"
			else:
				resp = "That functionality is not available for regular users."
		elif re.search("^bad resp.*", input):
			# Track info about bad responses
			msg = "Saving bad response state."
			if socket is None:
				print msg
			else:
				try:
					socket.sendall(msg+"\n")
				except:
					pass
			try:
				fp = open(self.bad_response_filename, "a")
				fp.write("Response declared bad, state was:\n")
				fp.write("Time: "+ time.strftime("%Y%m%d %H:%M:%S", time.localtime(time.time())) +"\n")
				fp.write("Input 2: "+ self.brains[sb].getInput(index = 2) +"\n")
				fp.write("That 2: "+ self.brains[sb].getThat(index = 2) +"\n")
				fp.write("Input 1: "+ self.brains[sb].getInput(index = 1) +"\n")
				fp.write("That 1: "+ self.brains[sb].getThat(index = 1) +"\n")
				fp.write("--------------------------\n")
				fp.close()
			except:
				print "Error storing bad response to file: "+ self.bad_response_filename
			resp = "Bad response acknowledged."
		elif re.search("^!\S*", input):
			text = re.sub("^!", "", input)
			os.system(text)
			resp = "Exec'd command \""+ text +"\"."
		elif re.search("^saytext \S*", input) or re.search("^readtext \S*", input):
			if re.search("^saytext \S*", input):
				text = re.sub("^saytext ", "", input)
			else:
				text = re.sub("^readtext ", "", input)
			vThread.say(text)
			resp = "saidtext."
		elif input == "load bot defaults" or input == "loadbotdefaults":
    			self.loadBotDefaults()
		elif re.search("^echob .*", input) or re.search("^echo bot .*", input):
			input = re.sub("^echo bot ", "echob ", input)
			key = re.sub("^echob ", "", input)
			value = self.getBotValue(key)
			resp = "bot "+ key +" = ("+ value +")"
		elif re.search("^echo .*", input):
			key = re.sub("^echo ", "", input)
			if key == "that" or key == "output":
				value = self.brains[sb].getThat()
		        elif re.search("^that=.*", key) or re.search("^output=.*", key):
				subindex = int(re.sub("^.*=", "", key))
				value = self.brains[sb].getThat(index=1, sentence_index=subindex)
			elif key == "input":
				value = self.brains[sb].getInput()
			elif key == "mthresh":
				value = str(self.mood_threshold)
			elif key == "vocal" or key == "vocalize":
				value = str(self.vocalize)
			elif key == "showsrais":
				value = str(self.showSrais)
			elif key == "showmatches":
				value = str(self.showMatches)
			elif key == "debugpath":
				value = str(self.showSrais) +", "+ str(self.showMatches)
				key = "showsrais, showmatches"
			elif key == "debugresponses":
				value = str(self.debugResponses)
			elif key == "detectrepeats" or key == "detectrepetition":
				value = str(self.useRepetitionDetector)
			elif key == "loi" or key == "level_of_interest":
				value = self.loi
			elif key == "inputhistory" or key == "input_history":
				value = string.join(self.getInputHistory(sb=sb), ", ")
			elif key == "outputhistory" or key == "output_history":
				value = string.join(self.getOutputHistory(sb=sb), ", ")
			elif key == "profiledata" or key == "profile_data":
				value = "Profile data:\n  patterns: %d\n  edges: %d\n  srais: %d\n  catchalls: %d\n  goap responses: %d" % (self.profile_data['num_patterns'], self.profile_data['num_edges'], self.profile_data['num_srais'], self.profile_data['num_catchalls'], self.profile_data['num_goap_responses'])
			else:
				value = self.getValue(key)
			resp = key +" = ("+ str(value) +")"
		elif re.search("^vartime .*", input):
			key = re.sub("^vartime ", "", input)
			resp = key +" = ("+ str(self.getValueSetTime(key)) +")"
		elif re.search("^setlastseen .*", input):
			key = re.sub("^setlastseen ", "", input)
			self.saveLastTimeThisName(key)
			resp = "setlastseen ("+ key +"): ("+ str(self.getLastTimeThisName(key)) +")"
		elif re.search("^lastseen .*", input):
			key = re.sub("^lastseen ", "", input)
			resp = "lastseen ("+ key +"): ("+ str(self.getLastTimeThisName(key)) +")"
		elif re.search("^previousseen .*", input):
			key = re.sub("^previousseen ", "", input)
			resp = "previousseen ("+ key +"): ("+ str(self.getPreviousTimeThisName(key)) +")"
		elif re.search("^setb \S* = .*", input) or re.search("^set bot \S* = .*", input):
			input = re.sub("^set bot ", "setb ", input)
			key = re.sub("^setb *", "", input)
			key = re.sub(" = .*", "", key)
			value = re.sub("^setb \S* = ", "", input)
			# A value of "" doesn't mean two quotes, it
			# means empty string.
			if value == "\"\"":
				value = ""
			self.setBotValue(key, value)
			# with a multi-word name, set firstname and lastname as well.
			if key == "name" and re.search("\s", value):
				name_values = re.split("\s", value)
				self.setBotValue("firstname", name_values[0])
				# name will only be firstname
				self.setBotValue("name", name_values[0])
				if len(name_values) > 2:
					self.setBotValue("middlename", string.join(name_values[1:-1], " "))
				if len(name_values) > 1:
					self.setBotValue("lastname", name_values[-1])
			# with a one-word name, set the firstname as well.
			if key == "name" and not re.search("\s", value):
				self.setBotValue("firstname", value)
			# setting/unsetting graphvizfile does a little more
			# than just setting/unsetting the bot variable
			if key == "graphvizfile":
				self.setGraphvizFile(value)
				if self.graphviz_file != "":
					# initialize the file and turn on track_match_path
					try:
						fp = open(self.graphviz_file, "w")
						fp.close()
						for tsb in self.brains:
							# don't reset match path here, make the user do that themselves
							self.brains[tsb].track_match_path = True
					except Exception, msg:
						print "Error creating graphviz file %s (%s)" % (self.graphviz_file, msg)
						self.graphviz_file = ""
						for tsb in self.brains:
							self.brains[tsb].track_match_path = False
				else:
					# turn off track_match_path
					for tsb in self.brains:
						self.brains[tsb].track_match_path = False
			resp = "set bot "+ key +" = ("+ value +")"
		elif re.search("^set \S* = .*", input):
			key = re.sub("^set *", "", input)
			key = re.sub(" = .*", "", key)
			value = re.sub("^set \S* = ", "", input)
			# A value of "" doesn't mean two quotes, it
			# means empty string.
			if value == "\"\"":
				value = ""
			if key == "mthresh":
				try:
					self.setMoodThreshold(string.atof(value))
				except Exception, msg:
					pass
			elif key == "mood_decay_value" or key == "mdecay":
				try:
					self.mood_decay_value(string.atof(value))
				except Exception, msg:
					pass
			elif key == "vocal" or key == "vocalize":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				key = "vocalize"
				self.setVocalize(bvalue)
			elif key == "showsrais":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowSrais(bvalue)
			elif key == "showmatches":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowMatches(bvalue)
			elif key == "debugpath":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowMatches(bvalue)
				self.setShowSrais(bvalue)
			elif key == "debugresponses":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setDebugResponses(bvalue)
			elif key == "debuggoap":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				gThread.setDebugGOAP(bvalue)
			elif key == "detectrepeats" or key == "detectrepetition":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				key = "detectrepeats"
				self.useRepetitionDetector = bvalue
			elif key == "loi" or key == "level_of_interest":
				if string.lower(value) == "low" or string.lower(value) == "l":
					self.loi = "low"
					value = "low"
				elif string.lower(value) == "high" or string.lower(value) == "h":
					self.loi = "high"
					value = "high"
				else:
					self.loi = "neutral"
					value = "neutral"
				key = "loi"
			elif re.search("^bot_mood_.*", key):
				key = re.sub("^bot_mood_", "", key)
				try:
					self.moods[key] = string.atof(value)
					self.setBotMoodFromMoods()
					self.saveMoodsToBrain(sb = sb)
				except:
					pass
			else:
				self.setValue(key, value)
			resp = "set "+ key +" = ("+ value +")"
		elif re.search("^debugfile .*", input):
			value = re.sub("^debugfile ", "", input)
			value = re.sub("^= ", "", value)
			if value == "nil" or value == "null":
				value = ""
			self.brains[sb].setWatchFile(value)
			resp = "debugfile = ("+ value +")"
		elif re.search("^savebrainstate \S*", input):
			brainname = re.sub("^savebrainstate ", "", input)
			if self.saveBrainState(brainname):
				resp = "Error saving brainstate to file: "+ brainname
			else:
				resp = "Brainstate saved to file: "+ brainname
		elif re.search("^savebrain \S*", input):
			brainname = re.sub("^savebrain ", "", input)
			if self.saveBrain(brainname):
				resp = "Error saving brain to file: "+ brainname
			else:
				resp = "Brain saved to file: "+ brainname
		elif re.search("^loadbrainstate \S*", input):
			brainname = re.sub("^loadbrainstate ", "", input)
			if self.loadBrainState(brainname):
				resp = "Error loading brainstate "+ brainname
			else:
				resp = "Brainstate loaded from file: "+ brainname
		elif re.search("^loadbrain \S*", input):
			brainname = re.sub("^loadbrain ", "", input)
			if self.loadBrain(brainname):
				resp = "Error loading brain "+ brainname
			else:
				resp = "Brain loaded from file: "+ brainname
		elif re.search("^learn .*", input) or re.search("^debuglearn .*", input):
			do_debug = False
			if re.search("^debuglearn .*", input):
				do_debug = True
				input = re.sub("^debug", "", input)
			learnfile = re.sub("^learn ", "", input)
			retval = 1
			do_load_aiml_b = True
			# if the learnfile begins with a "+" or does NOT
			# end in ".xml", don't do load_aiml_b....
			if re.search("^\+", learnfile) or not re.search("\.xml$", learnfile):
				learnfile = re.sub("^\+", "", learnfile)
				do_load_aiml_b = False
			resp = ""
			if do_debug:
				self.brains[sb].setWatchFile(learnfile)
				resp = "debugfile = ("+ learnfile +")\n"
			retval = self.learnFile(learnfile, do_load_aiml_b)
			if retval:
				resp += "Error loading learn file "+ learnfile
			else:
				resp += "learned file: "+ learnfile
		elif re.search("^exec \S*", input):
			cmd_args = string.split(re.sub("^exec ", "", input), " ")
			cmd = cmd_args.pop(0)
			eThread.execute(cmd, cmd_args)
		elif re.search("^wordnet add [^:]+:\S*", input):
			# format is "wordnet add dog:1:def1.1,def1.2; 2:def2.1,def2.2,def2.3"
			input = re.sub("^wordnet add ", "", input)
			wordnet = re.sub("^([^:]+):.*", "\\1", input)
			wordnet_def = []
			defstring = string.split(re.sub("^[^:]+:", "", input), ";")
			for thisdef in defstring:
				thisdef = thisdef.strip()
				thisdefnum = 1
				if re.search("^[0-9]+:\S*", thisdef):
					thisdefnum = string.atoi(re.sub("^([0-9]+):.*", "\\1", thisdef))
					thisdef = re.sub("^[0-9]+:", "", thisdef)
				if thisdefnum < 1:
					thisdefnum = 1
				# extend the array as necessary
				while thisdefnum > len(wordnet_def):
					wordnet_def.append( [] )
				defarray = string.split(thisdef, ",")
				print "def %d of %s is (%s)" % (thisdefnum, wordnet, str(defarray))
				for td in defarray:
					wordnet_def[thisdefnum - 1].append(td.strip())
			if len(wordnet_def) > 0:
				retval = self.addWordnet(wordnet, wordnet_def)
				resp = "wordnet: added definitions for ("+ wordnet +") = ("+ str(wordnet_def) +")."
			else:
				resp = "wordnet: No definitions to add."
		elif re.search("^write match path", input) or re.search("^save match path", input):
			wmpretval = self.writeMatchPath(sb)
			if wmpretval == -1:
				resp = "error writing out match path"
			elif wmpretval == 0:
				resp = "nothing to write for match path"
			elif wmpretval == 1:
				resp = "match path has been written"
		elif re.search("^reset match path", input) or re.search("^clear match path", input):
			self.brains[sb].resetMatchPath()
			resp = "match path has been reset"
		elif re.search("^memoryfootprint.*", input) or re.search("^memfootprint.*", input):
			key = "all"
			if re.search("^memoryfootprint .*", input) or re.search("^memfootprint .*", input):
				key = re.sub("^memfootprint ", "memoryfootprint ", input)
			if key == "all":
				value = sys.getsizeof(self.brains[sb]._botPredicates)
				resp = "memory footprint for _botPredicates = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb]._sessions)
				resp = resp +"\nmemory footprint for _sessions = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb].live_gossip_templates)
				resp = resp +"\nmemory footprint for live_gossip_templates = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb].added_categories)
				resp = resp +"\nmemory footprint for added_categories = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb]._brain._root)
				resp = resp +"\nmemory footprint for _root = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb]._brain._topictree_root)
				resp = resp +"\nmemory footprint for _topictree_root = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb]._brain._thattree_root)
				resp = resp +"\nmemory footprint for _thattree_root = %d bytes" % (value)
				value = sys.getsizeof(self.brains[sb]._brain._wordtree_root)
				resp = resp +"\nmemory footprint for _wordtree_root = %d bytes" % (value)
			else:
				value = -1
				if key == "_botPredicates":
					value = sys.getsizeof(self.brains[sb]._botPredicates)
				elif key == "_sessions":
					value = sys.getsizeof(self.brains[sb]._sessions)
				elif key == "live_gossip_templates":
					value = sys.getsizeof(self.brains[sb].live_gossip_templates)
				elif key == "added_categories":
					value = sys.getsizeof(self.brains[sb].added_categories)
				elif key == "_root":
					value = sys.getsizeof(self.brains[sb]._brain._root)
				elif key == "_topictree_root":
					value = sys.getsizeof(self.brains[sb]._brain._topictree_root)
				elif key == "_thattree_root":
					value = sys.getsizeof(self.brains[sb]._brain._thattree_root)
				elif key == "_wordtree_root":
					value = sys.getsizeof(self.brains[sb]._brain._wordtree_root)
				resp = "memory footprint for %s = %d bytes" % (key, value)
		elif re.search("^learngossip *", input):
			# pass the string to _processGossip() so it can
			# generate the appropriate gossip based on that
			# sentence. 
			thisgossip = re.sub("^learngossip ", "", input)
			resp = self.learnGossip(thisgossip, sb)
			resp = "learngossip: (%s)" % (resp)
		else:
			self.temp_vocalize = False
			if re.search("^vocal: .*", input):
				input = re.sub("^vocal: ", "", input)
				self.temp_vocalize = True
			last_name_state = self.getValue("name", sb = sb)
			## If we have a name, remember we're talking
			## to them.  Don't force the save, though.
			if last_name_state != "":
				self.saveLastTimeThisName(last_name_state)
			resp = self.brains[sb].respond(input)
			## update profiling patterns
			new_profile_data = self.brains[sb].characterizeProfilingPatterns()
			self.profile_data['num_patterns'] = self.profile_data['num_patterns'] + new_profile_data['num_patterns']
			self.profile_data['num_edges'] = self.profile_data['num_edges'] + new_profile_data['num_edges']
			self.profile_data['num_srais'] = self.profile_data['num_srais'] + new_profile_data['num_srais']
			self.profile_data['num_catchalls'] = self.profile_data['num_catchalls'] + new_profile_data['num_catchalls']
			if last_name_state != self.getValue("name", sb = sb):
				## Name has changed, force save of the current
				## name and the new name.
				self.saveLastTimeThisName(last_name_state, True)
				self.saveLastTimeThisName(self.getValue("name", sb = sb), True)
			## Retrieve the level of interest from the brain.
			## This is used to set the default level of interest
			## for the most recent input.  The actual level of
			## interest that the bot will remember is based on
			## other factors, including directional changes in
			## mood, and repetition.
			proposed_loi = self.getValue("level_of_interest", sb = sb)
			## Get predicate mood_plus, if it has a value
			## increase that value's counter.
			## Check mood_minus as well, decreasing its value
			## mood_rplus, mood_rminus add or remove 0 or 1
			## at random.  mood_rmod is random -1, 0, or 1
			## Then set these to an empty string.
			mood = self.getValue("mood_plus", sb = sb)
			if mood != "":
				if self.debugResponses:
					resp = "Increasing mood: "+ mood +"\n"+ resp
				self.increaseMood(mood)
				proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, 1)
			mood = self.getValue("mood_minus", sb = sb)
			if mood != "":
				if self.debugResponses:
					resp = "Decreasing mood: "+ mood +"\n"+ resp
				self.decreaseMood(mood)
				proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, -1)
			mood = self.getValue("mood_rplus", sb = sb)
			if mood != "":
				rint = random.randint(0, 1)
				if rint > 0:
					if self.debugResponses:
						resp = "Increasing mood: "+ mood +"\n"+ resp
					self.increaseMood(mood)
					proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, rint)
			mood = self.getValue("mood_rminus", sb = sb)
			if mood != "":
				rint = random.randint(0, 1)
				if rint > 0:
					if self.debugResponses:
						resp = "Decreasing mood: "+ mood +"\n"+ resp
					self.decreaseMood(mood)
					proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, rint)
			mood = self.getValue("mood_rmod", sb = sb)
			if mood != "":
				rint = random.randint(-1, 1)
				if rint > 0:
					if self.debugResponses:
						resp = "Increasing mood: "+ mood +"\n"+ resp
					self.increaseMood(mood)
					proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, rint)
				elif rint < 0:
					if self.debugResponses:
						resp = "Decreasing mood: "+ mood +"\n"+ resp
					self.decreaseMood(mood)
					proposed_loi = self.setProposedLOIFromMoodDirection(proposed_loi, mood, rint)
			self.setValue("mood_plus", "")
			self.setValue("mood_minus", "")
			self.setValue("mood_rplus", "")
			self.setValue("mood_rminus", "")
			self.setValue("mood_rmod", "")
			# detect repetition of input or output
			detectedInputRepetition = False
			detectedOutputRepetition = False
			# the fake input that calls the GOAP pattern is not
			# considered repetition, neither is the secrettasktag
			if input.upper() != "CURRENTGOAPPATTERN" and input.upper() != "SECRETTASKTAG":
				inputHistory = self.getInputHistory(sb=sb)
				inputHistoryCount = {}
				for ih in inputHistory:
					if not inputHistoryCount.has_key(ih):
						inputHistoryCount[ih] = 0
					inputHistoryCount[ih] = inputHistoryCount[ih] + 1
				# we've been asked this question too many times
				if inputHistoryCount[ inputHistory[-1] ] > 2:
					print "Repetition detected on ("+ inputHistory[-1] +")"
					proposed_loi = "low"
					detectedInputRepetition = True
			outputHistory = self.getOutputHistory(sb=sb)
			outputHistoryCount = {}
			for oh in outputHistory:
				if not outputHistoryCount.has_key(oh):
					outputHistoryCount[oh] = 0
				outputHistoryCount[oh] = outputHistoryCount[oh] + 1
			# be a little more flexible with same output (since 
			# "yes" and "no" will be common outputs but can come
			# from different inputs)
			if outputHistoryCount[ outputHistory[-1] ] > 2:
				#print "Too many same responses ("+ outputHistory[-1] +")"
				proposed_loi = "low"
				detectedOutputRepetition = True
			get_goap_resp = None
			if detectedInputRepetition:
				if self.useRepetitionDetector:
					responses = ["You've asked that question before.", "Let's talk about something else."]
					randresp = random.randint(1, len(responses)) - 1
					resp = responses[randresp]
					self.replaceLastElementOfOutputHistory(resp, sb = sb)
				else:
					print "Repetition detector is off."
			elif detectedOutputRepetition:
				if self.useRepetitionDetector:
					if input == "CurrentGOAPPattern":
						if re.search("\?$", resp):
							responses = ["How come you won't answer my question?", "Why won't you answer my question"]
							randresp = random.randint(1, len(responses)) - 1
							resp = responses[randresp]
						else:
							responses = ["Maybe we can talk about something else.", "This conversation is starting to get a little boring.", "Let's talk about something else."]
							randresp = random.randint(1, len(responses)) - 1
							resp = responses[randresp]
						get_goap_resp = False
					else:
						responses = ["I'm getting bored.", "Let's talk about something else."]
						randresp = random.randint(1, len(responses)) - 1
						resp = responses[randresp]
					self.replaceLastElementOfOutputHistory(resp, sb = sb)
				else:
					print "Repetition detector is off."
			self.processProposedLOI(proposed_loi)
			loi_info = "Proposed LOI: none (actual: "+ self.loi +")"
			if proposed_loi != "":
				loi_info = "Proposed LOI: "+ proposed_loi +" (actual: "+ self.loi +")"
			# only print LOI information if it's interesting
			if proposed_loi != "" or self.loi != "neutral":
				print loi_info
			# reset the brain's level of interest
			self.setValue("level_of_interest", "")
			# if we haven't made a determination about using a 
			# GOAP response yet, consult the LOI value
			if get_goap_resp == None:
				get_goap_resp = False
				if self.loi == "low":
					get_goap_resp = True
			
		####################
		## Process responses
		####################
		if re.search("^::EXEC_TASK::", resp):
			# Request to run a task
			if self.debugResponses:
				print "Resp has exec %s" % resp
			task = re.sub("^::EXEC_TASK::", "", resp)
			task_args = []
			if re.search("::", task):
				task_args = string.split(re.sub("^[^:]+::", "", task),"::")
				task = re.sub("::.*", "", task)
			if self.debugResponses:
				resp = "Executing task: "+ task +" "+ string.join(task_args, ", ")
			else:
				resp = ""
			runAIMLTask(task, task_args, self.temp_vocalize, socket = socket)
		elif input == "CurrentGOAPPattern":
			# Outputting a GOAP response
			if self.debugResponses:
				resp = "GOAP response:: "+ resp
		elif has_goap and get_goap_resp:
			# Changing non-GOAP response into a GOAP response
			if self.debugResponses:
				print "Consulting GOAP module to determine what to say."
			self.pushBrainVariablesToGOAPThread()
			goap_response = gThread.get_bot_response()
			if gThread.debugGOAP:
				print "GOAP:: Old response which is being overwritten by GOAP response was ("+ resp +")"
			if goap_response == "":
				# don't overwrite resp.  just respond with whatever the brain already produced
				if gThread.debugGOAP:
					print "GOAP failed to generate a response.  Allowing the AIML brain to process responses again and returning the current response from the AIML brain."
				print "GOAP module has no suggestions."
				self.loi = "neutral"
				# and don't try GOAP again for a little while
				self.processProposedLOI(self.loi)
			else:
				goap_template = {"template": [goap_response] }
				self.addGOAPCategoryFromDraftTemplate(goap_template, sb = sb)
				resp = self.process_input("CurrentGOAPPattern", sb, socket)
				self.num_sequential_goap_questions = self.num_sequential_goap_questions + 1
				# FIXME: really this should only check if we aren't getting answers to the GOAP brain's questions.  So if the number of unsatisfied goals remains the same as the last time, then check this limit, otherwise allow us to continue the GOAP-driven conversation
				# but for now it's just a simple limit
				if self.num_sequential_goap_questions > self.max_sequential_goap_questions:
					self.loi = "neutral"
					# don't try GOAP again for a little while
					self.processProposedLOI(self.loi)
				self.profile_data['num_goap_responses'] = self.profile_data['num_goap_responses'] + 1
		else:
			# Normal, non-GOAP response
			self.num_sequential_goap_questions = 0
		self.adjustGOAPGoals(self.brains[sb].getPredicate("__array_goapgoals"), self.brains[sb].getPredicate("__array_goapgoalremovals")) 
		# reinitialize the goals each time
		self.brains[sb].setPredicate("__array_goapgoals", [])
		self.brains[sb].setPredicate("__array_goapgoalremovals", [])
		#print "Bot's goals are (%s)" % (self.getGOAPGoals())
		# overwrite the AIML's view of the current goal set based
		# on what the bot's goals actually are
		new_goal_set = self.getGOAPGoalsAsStrings()
		self.brains[sb].setPredicate("__array_goapgoals", deepcopy(new_goal_set))
		return resp

	def getGOAPGoals(self):
		return gThread.getGoals()

	def getGOAPGoalsAsStrings(self):
		return gThread.getGoalsAsStrings()

	def adjustGOAPGoals(self, goals, goalremovals):
		gThread.addGoals(deepcopy(goals))
		gThread.removeGoals(deepcopy(goalremovals))

	def learnGossip(self, thisgossip, sb = "__main"):
        	# ensure that thisgossip is a unicode string
        	try: input = thisgossip.decode(self.brains[sb]._textEncoding, 'replace')
        	except UnicodeError: pass
        	except AttributeError: pass
		if len(input) == 0:
            		return ""
		##
		# Begin: Manipulate input like we'd do on a respond()
		# replace some periods before breaking up the input
		input = self.brains[sb].PeriodFilter(input)
            	##
            	# HLPC: handle parens
            	# remove parens if entire input is contained in them
            	# and if it contains no more parens:
            	input = re.sub("^\(([^\(\)]*)\)$","\\1", input)
            	# remove parens if we start with an open, but contain no more parens
            	input = re.sub("^\(([^\(\)]*)$","\\1", input)
            	# remove parens if we start with a close, but contain no more parens
            	input = re.sub("^\)([^\(\)]*)$","\\1", input)
            	#
            	##
            	# then strip the punctuation
            	input = re.sub("[\?\!\.;]$","", input)
        	# run the input through the 'normal' subber
        	subbedInput = self.brains[sb]._subbers['normal'].sub(input)
        	# HLPC: then replace "'S\b" with " S\b" since the subber will
        	# HLPC: catch "it's nice" but not "my dog's name is ..."
        	subbedInput = re.sub("'([sS])\\b", " \\1", subbedInput)
		# End: Manipulate input
		##
		elem = []
		elem_one_dict = {}
		meta_dict = {"topic" : u'*', "pattern" : subbedInput, "line" : 0, "file" : u'manual:gossip', "that" : u'*'}
		elem_one_dict["meta"] = meta_dict
		elem_two_array = ['text', {'xml:space': 'default'}, subbedInput]
		elem.append('template')
		elem.append(elem_one_dict)
		elem.append(elem_two_array)
		return self.brains[sb]._processGossip(elem, sessionID = self.brains[sb]._globalSessionID)

	def pushBrainVariablesToGOAPThread(self, sb = "__main"):
		for key in self.brains[sb].getPredicateKeys():
			gThread.addTagFromPair(key, deepcopy(self.brains[sb].getPredicate(key)))

	def setProposedLOIFromMoodDirection(self, proposed_loi, mood, direction):
		# If there's already a proposed LOI, just use that
		if proposed_loi != "":
			return proposed_loi
		# Otherwise consult the mood and its direction
		if direction > 0:
			if mood == "flattered":
				proposed_loi = "high"
		elif direction < 0:
			if mood == "flattered":
				proposed_loi = "low"
		else:
			proposed_loi = "neutral"
		return proposed_loi

	def processProposedLOI(self, proposed_loi):
		# Expect low, neutral/medium, high
		if proposed_loi != "":
			self.loi_history.append(proposed_loi)
		# track a limited history of levels of interest
		while len(self.loi_history) > self.loi_history_max_length:
			self.loi_history.pop(0)
		# use this information to set our LOI
		loi_count = 0;
		low_loi_count = 0;
		high_loi_count = 0;
		i = 0
		while i < len(self.loi_history):
			loi_count = loi_count + 1
			if self.loi_history[i] == "low":
				low_loi_count = low_loi_count + 1
			elif self.loi_history[i] == "high":
				high_loi_count = high_loi_count + 1
			i = i + 1
		if loi_count < 3:
			return
		self.loi = "neutral"
		if float(low_loi_count) / float(loi_count) >= 0.66:
			self.loi = "low"
		elif float(high_loi_count) / float(loi_count) >= 0.66:
			self.loi = "high"

	def addWordnet(self, wordnet, wordnet_def, sb = "__main"):
		self.brains[sb].addWordnet(wordnet, wordnet_def)
		return

	def addGOAPCategoryFromDraftTemplate(self, drafttemplate = {"template": ""}, sb = "__main"):
		if drafttemplate["template"] == "" and self.drafttemplate["template"] != "":
			drafttemplate = self.drafttemplate
		if drafttemplate["template"] == "":
			return
		drafttemplate["pattern"] = "CURRENTGOAPPATTERN"
		temp_aiml_filename = self.brains["__main"].createTempAIMLFileFromDraftCategory(drafttemplate)
		if temp_aiml_filename != "":
			added_patterns = self.brains["__main"].learn(temp_aiml_filename)
			# added_patterns will be blank because remember_categories is false
			os.unlink(temp_aiml_filename)
			print "Loaded current GOAP pattern."
		return
	def deleteCategoryFromDraftTemplate(self, drafttemplate = {"pattern": ""}, sb = "__main"):
		if drafttemplate["pattern"] == "" and self.drafttemplate["pattern"] != "":
			drafttemplate = self.drafttemplate
		if drafttemplate["pattern"] == "":
			return
		temp_aiml_filename = self.brains["__main"].createTempAIMLFileFromDraftCategory(drafttemplate)
		if temp_aiml_filename != "":
			added_patterns = self.brains["__main"].learn(temp_aiml_filename, remember_categories = False)
			for key,tem in added_patterns.items():
				self.live_templates[key] = tem
			if len(added_patterns) >= 1:
				os.unlink(temp_aiml_filename)
				print "Deleted pattern."
			else:
				print "Error deleting patterns in %s." % (temp_aiml_filename)
		return
	def addCategoryFromDraftTemplate(self, drafttemplate = {"pattern": ""}, sb = "__main"):
		if drafttemplate["pattern"] == "" and self.drafttemplate["pattern"] != "":
			drafttemplate = self.drafttemplate
		if drafttemplate["pattern"] == "":
			return
		temp_aiml_filename = self.brains["__main"].createTempAIMLFileFromDraftCategory(drafttemplate)
		if temp_aiml_filename != "":
			added_patterns = self.brains["__main"].learn(temp_aiml_filename, remember_categories = True)
			for key,tem in added_patterns.items():
				self.live_templates[key] = tem
			if len(added_patterns) >= 1:
				os.unlink(temp_aiml_filename)
				print "Newly added template is in "+ temp_aiml_filename
			else:
				print "Error loading live template patterns in %s." % (temp_aiml_filename)
		return
	def processBotValues(self, sb = "__main"):
		for key in self.botvalues:
			self.setBotValue(key, self.botvalues[key], sb = sb)
	def processNormalValues(self, sb = "__main"):
		for key in self.normalvalues:
			self.setValue(key, self.normalvalues[key], sb = sb)
	def getBotName(self, sb = "__main"):
		return self.botvalues["name"]
	def setBotName(self, value, sb = "__main"):
		name_values = re.split("\s", value)
		middlename = ""
		lastname = ""
		firstname = name_values[0]
		if len(name_values) > 2:
			middlename = string.join(name_values[1:-1], " ")
		if len(name_values) > 1:
			lastname = name_values[-1]
		self.botvalues["name"] = value
		self.setBotValue("name", value, sb)
		self.setBotFullName(firstname, middlename, lastname, sb)
	def setBotFullName(self, firstname, middlename, lastname, sb = "__main"):
		self.botvalues["firstname"] = firstname
		self.setBotValue("firstname", firstname, sb)
		self.botvalues["middlename"] = middlename
		self.setBotValue("middlename", middlename, sb)
		self.botvalues["lastname"] = lastname
		self.setBotValue("lastname", lastname, sb)
		self.botvalues["name"] = firstname
		self.setBotValue("name", firstname, sb)
	def setBotNickName(self, nickname, sb = "__main"):
		self.botvalues["nickname"] = nickname
		self.setBotValue("nickname", nickname, sb)
	def getBotMaster(self, sb = "__main"):
		return self.botvalues["master"]
	def setBotMaster(self, value, sb = "__main"):
		self.botvalues["master"] = value
		self.setBotValue("master", value, sb)
	def setValue(self, key, value, sb = "__main"):
		self.brains[sb].setPredicate(key, str(value))
	def getValue(self, key, sb = "__main"):
		return self.brains[sb].getPredicate(key)
	def getInputHistory(self, sb = "__main"):
        	return self.brains[sb].getPredicate(self.brains[sb]._inputHistory)
	def getOutputHistory(self, sb = "__main"):
        	return self.brains[sb].getPredicate(self.brains[sb]._outputHistory)
	def replaceLastElementOfOutputHistory(self, response, sb = "__main"):
        	self.brains[sb].replaceLastElementOfOutputHistory(response)
	def getValueSetTime(self, key, sb = "__main"):
		return self.brains[sb].getPredicateSetTime(key)
	def setBotValue(self, key, value, sb = "__main"):
		self.brains[sb].setBotPredicate(key, str(value))
	def getBotValue(self, key, sb = "__main"):
		return self.brains[sb].getBotPredicate(key)
	def saveBrainState(self, brainstatename, sb = "__main"):
		# Save state only, not patterns
		if not re.search("^.*\.brainstate$", brainstatename):
			brainstatename = brainstatename + ".brainstate"
		if sb in self.brains:
			# make sure loi and loi_history get saved
			self.brains[sb].setPredicate("__saved_loi", self.loi)
			self.brains[sb].setPredicate("__saved_loi_history", self.loi_history)
			self.brains[sb].setPredicate("__saved_repetitiondetector", self.useRepetitionDetector)
			self.brains[sb].verbose(True)
			self.brains[sb].saveBrainState(brainstatename)
			self.brains[sb].verbose(False)
			return 0
		else:
			return 1
	def saveBrain(self, brainname, sb = "__main"):
		# Save patterns and state
		if sb in self.brains:
			# make sure loi and loi_history get saved
			self.brains[sb].setPredicate("__saved_loi", self.loi)
			self.brains[sb].setPredicate("__saved_loi_history", self.loi_history)
			self.brains[sb].setPredicate("__saved_repetitiondetector", self.useRepetitionDetector)
			self.brains[sb].verbose(True)
			self.brains[sb].saveBrain(brainname)
			self.brains[sb].verbose(False)
			return 0
		else:
			return 1
	def loadBrainState(self, brainstatename, sb = "__main"):
		# Load state only, not patterns
		if not re.search("^.*\.brainstate$", brainstatename):
			brainstatename = brainstatename + ".brainstate"
		if os.path.exists(brainstatename):
			if not sb in self.brains:
				self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(True)
			self.brains[sb].loadBrainState(brainstatename)
			self.brains[sb].verbose(False)
			self.setStateVariablesFromBrain(sb)
			return 0
		else:
			return 1
	def loadBrain(self, brainname, sb = "__main"):
		# Load patterns and state
		if os.path.exists(brainname):
			if not sb in self.brains:
				self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(True)
			self.brains[sb].loadBrain(brainname)
			self.brains[sb].verbose(False)
			self.setStateVariablesFromBrain(sb)
			return 0
		else:
			return 1

	def setStateVariablesFromBrain(self, sb = "__main"):
		if self.dbh != None:
			self.brains[sb].setDBConnection(self.dbh)
		# now adjust the GOAP thread's goals based on what
		# was saved in the brainstate
		self.adjustGOAPGoals(self.brains[sb].getPredicate("__array_goapgoals"), self.brains[sb].getPredicate("__array_goapgoalremovals")) 
		self.brains[sb].setPredicate("__array_goapgoals", [])
		self.brains[sb].setPredicate("__array_goapgoalremovals", [])
		new_goal_set = self.getGOAPGoalsAsStrings()
		self.brains[sb].setPredicate("__array_goapgoals", deepcopy(new_goal_set))
		# and adjust loi and loi_history from brainstate
		self.loi = self.brains[sb].getPredicate("__saved_loi")
		self.loi_history = self.brains[sb].getPredicate("__saved_loi_history")
		# and debugpath and repetition detector
		bvalue = self.brains[sb].showMatches
		self.setShowMatches(bvalue)
		bvalue = self.brains[sb].showSrais
		self.setShowSrais(bvalue)
		bvalue = self.brains[sb].getPredicate("__saved_repetitiondetector")
		self.useRepetitionDetector = bvalue
		# and moods from brainstate
		mood_list = self.brains[sb].getPredicate("__array_bot_mood_list")
		try:
			for mood in mood_list:
				self.moods[mood] = string.atof(self.brains[sb].getPredicate("bot_mood_"+mood))
		except:
			pass

	def learnFile(self, learnfile, do_load_aiml_b = True, sb = "__main", verbose = True, savePatterns = False):
		if not sb in self.brains:
			self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(False)
			self.brains[sb].setUseHLPCTrees(self.useHLPCtrees)
		oldverbose = self.brains[sb].verbose()
		# FIXME: At least for debugging, check in the aiml/hlpc
		# FIXME: directory if there's no file found.
		# FIXME: And why not append ".aiml" too.
		if not os.path.exists(learnfile):
			if os.path.exists("aiml/hlpc/"+ learnfile):
				learnfile = "aiml/hlpc/"+ learnfile
			elif os.path.exists(learnfile +".aiml"):
				learnfile = learnfile +".aiml"
			elif os.path.exists("aiml/hlpc/"+ learnfile +".aiml"):
				learnfile = "aiml/hlpc/"+ learnfile +".aiml"
		if os.path.exists(learnfile):
			if verbose == True:
				self.brains[sb].verbose(True)
			added_patterns = self.brains[sb].learn(learnfile)
			if savePatterns:
				for key,tem in added_patterns.items():
					self.dynpatterns[key] = tem
			if do_load_aiml_b:
				self.brains[sb].respond("load aiml b")
			self.brains[sb].verbose(oldverbose)
			return 0
		else:
			return 1
	def learnGossipFile(self, learnfile, do_load_aiml_b = True, sb = "__main", verbose = True, saveGossipPatterns = False):
		if not sb in self.brains:
			self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(False)
			self.brains[sb].setUseHLPCTrees(self.useHLPCtrees)
		oldverbose = self.brains[sb].verbose()
		# FIXME: At least for debugging, check in the aiml/hlpc
		# FIXME: directory if there's no file found.
		# FIXME: And why not append ".aiml" too.
		if not os.path.exists(learnfile):
			if os.path.exists("aiml/hlpc/"+ learnfile):
				learnfile = "aiml/hlpc/"+ learnfile
			elif os.path.exists(learnfile +".aiml"):
				learnfile = learnfile +".aiml"
			elif os.path.exists("aiml/hlpc/"+ learnfile +".aiml"):
				learnfile = "aiml/hlpc/"+ learnfile +".aiml"
		if os.path.exists(learnfile):
			self.setGossipFile(learnfile, sb)
			if verbose == True:
				self.brains[sb].verbose(True)
			self.brains[sb].loadGossip(saveGossipPatterns)
			if do_load_aiml_b:
				self.brains[sb].respond("load aiml b")
			self.brains[sb].verbose(oldverbose)
			return 0
		else:
			return 1
	def setGossipFile(self, learnfile, sb = "__main"):
		self.gossip_file = learnfile
		self.brains[sb].setGossipFile(learnfile)
	def setVocalize(self, value):
		self.vocalize = value
	def setShowSrais(self, value, sb = "__main"):
		self.showSrais = value
		self.brains[sb].showSrais = value
	def setShowMatches(self, value, sb = "__main"):
		self.showMatches = value
		self.brains[sb].showMatches = value
	def setDebugResponses(self, value, sb = "__main"):
		self.debugResponses = value
	def new_aiml(self, aimltext, vocalize = False, socket = None):
		self.aiml_queue.append(aimltext)
		self.aiml_vocalize_queue.append(vocalize)
		self.aiml_socket_queue.append(socket)
		## FIXME:
		##        This should really not do this.  Same as
		##        new_async_response()
		##        Remove when run() is non-blocking.
		self.process_aiml_queue()
	def create_temp_aiml_file(self, new_aiml_template_text):
		temp_aiml_filename = "/tmp/tempaimlfile"+str(int(time.time()))+".aiml"
		try:
			fp = open(temp_aiml_filename, "w")
			fp.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
			fp.write("<aiml version=\"1.0\">\n")
			## <meta name="author" content="hlpc"/>
			## <meta name="language" content="en"/>
			fp.write("<category>\n")
			fp.write("<pattern>SECRETTASKTAG</pattern>\n")
			fp.write("<template>\n")
			fp.write(re.sub("&","&amp;",new_aiml_template_text))
			fp.write("</template>\n")
			fp.write("</category>\n")
			fp.write("</aiml>\n")
			fp.close()
		except:
			print "Error creating temporary aiml file: "+ temp_aiml_filename
			return ""
		return temp_aiml_filename
	def process_aiml_queue(self):
		sb = "__main"
		while len(self.aiml_queue) > 0:
			new_aiml_text = self.aiml_queue.pop(0)
			new_aiml_vocalize = self.aiml_vocalize_queue.pop(0)
			new_aiml_socket = self.aiml_socket_queue.pop(0)
			temp_aiml_file = self.create_temp_aiml_file(new_aiml_text)
			if temp_aiml_file == "":
				continue
			retval = self.learnFile(temp_aiml_file, False, sb = sb, verbose = False)
			os.unlink(temp_aiml_file)
			resp = self.process_input("secrettasktag", sb = sb, socket = new_aiml_socket)
			self.temp_vocalize = new_aiml_vocalize
			self.process_output(resp, sb = sb, socket = new_aiml_socket)
	def new_async_response(self, text, vocalize = False, socket = None):
		self.async_response_queue.append(text)
		self.async_vocalize_queue.append(vocalize)
		self.async_socket_queue.append(socket)
		## FIXME: 
		##        This should really not do this.  The run()
		##        method of this thread should handle printing
		##        but because it's blocking for now on user-input,
		##        this workaround will get the output printed.
		##        Remove when run() is non-blocking.
		self.print_async_response_queue()
	def print_async_response_queue(self):
		# These responses might need to be vocalized
		# or temp_vocalized (like EXEC_TASK)
		while len(self.async_response_queue) > 0:
			temp_vocalize = self.async_vocalize_queue.pop(0)
			temp_socket = self.async_socket_queue.pop(0)
			output = self.async_response_queue.pop(0)
			if temp_socket is None:
				print output
			else:
				try:
					temp_socket.sendall(output+"\n")
				except:
					pass
			if temp_vocalize:
				vThread.say(output, False)
	def saveLastTimeThisName(self, name, force = False):
		self.brains["__main"].saveLastTimeThisName(name, force)
	def getLastTimeThisName(self, name):
		return self.brains["__main"].getLastTimeThisName(name)
	def getPreviousTimeThisName(self, name):
		return self.brains["__main"].getPreviousTimeThisName(name)

class voiceThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.name = "voiceThread"
		self.stop_requested = False
		self.speech_queue = []
		self.verbose_queue = []
		self.exec_args = ["festival", "--pipe"]
		# Either:
		#self.voice = "nitech_us_slt_arctic_hts"
		# or:
		self.voice = "nitech_us_clb_arctic_hts"
	def run(self):
		sub_process = subprocess.Popen(self.exec_args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		try:
			sub_process.stdin.write("("+ self.voice +")")
		except:
			print "voiceThread:: Error setting voice for speech engine"
		# Wait for text to speak
		while not self.stop_requested:
			time.sleep(0.05)
			while len(self.speech_queue) > 0:
				text = self.speech_queue.pop(0)
				verbosity = self.verbose_queue.pop(0)
				if verbosity:
					print "Saying: "+ "(SayText \"" + text +"\")"
				try:
					sub_process.stdin.write("(SayText \"" + text +"\")")
				except:
					print "voiceThread:: Error sending text to speech engine"
	def stop(self):
		self.stop_requested = True
	def say(self, text, verbosity = True):
		self.speech_queue.append(text)
		self.verbose_queue.append(verbosity)

class goapThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.name = "goapThread"
		self.stop_requested = False
		self.botenv = None
		self.bot = None
		self.time = 0
		self.goap_requested = False
		self.debugGOAP = False
	def run(self):
		global aimlgoap_bot_path
		from aimlgoap.environmentaiml import AimlEnvironment
		from aimlgoap.agents import Bot
		from aimlgoap.goals import DefinedVariableGoal
		self.done = True
		self.time = 0

		self.botenv = AimlEnvironment()
            	self.bot = Bot("thisbot")
            	self.load_commands(self.bot, aimlgoap_bot_path)
            	self.bot.add_goal(DefinedVariableGoal("name"))
            	self.bot.add_goal(DefinedVariableGoal("gender"))
            	self.botenv.add_thing(self.bot)
		while not self.stop_requested:
			time.sleep(0.1)
		print "goap thread finished."
	def setDebugGOAP(self, value, sb = "__main"):
		self.debugGOAP = value
	def getGoals(self):
		from aimlgoap.goals import DefinedVariableGoal
		# return the current list of goals
            	return deepcopy(self.bot.goals)
	def getGoalsAsStrings(self):
		from aimlgoap.goals import DefinedVariableGoal
		# return the current list of goals as an array of strings
		goal_strings = []
		for goal in self.bot.goals:
		    if isinstance(goal, DefinedVariableGoal):
			goal_strings.append("DefinedVariableGoal(\"%s\")" % (goal.varname))
            	return goal_strings
	def addGoals(self, new_goals):
		from aimlgoap.goals import DefinedVariableGoal
		# add goal only if not already present
    		for this_goal in new_goals:
			found_goal = False
			actual_goal = eval(this_goal)
			for existing_goal in self.bot.goals:
				if str(actual_goal) == str(existing_goal):
					found_goal = True
					break
			# do not add duplicate goals
			if not found_goal:
            		    self.bot.add_goal(eval(this_goal))
	def removeGoals(self, old_goals):
		from aimlgoap.goals import DefinedVariableGoal
		# remove goal if present
    		for this_goal in old_goals:
			actual_goal = eval(this_goal)
			for existing_goal in self.bot.goals:
				if str(actual_goal) == str(existing_goal):
            				self.bot.remove_goal(existing_goal)
	def addTagFromPair(self, key, value):
		from pygoapng.blackboard import Tag
		tagkws = {}
		tagkws[key] = value
		tagkws['__keep_single_copy'] = 1
		tag = Tag(**tagkws)
		# add tags. __keep_single_copy only keeps a single
		# instance of each variable...
		self.bot.bb.post(tag)
	def get_bot_response(self):
		bot_response = ""
		self.goap_execute()
        	try:
                	bot_response = self.bot.current_response
        	except UnboundLocalError:
                	pass
		return bot_response
	def goap_execute(self):
		from pygoapng.agent import GoapAgent
		from pygoapng.blackboard import Tag
		if self.debugGOAP:
        		print "GOAP:: =============== STEP {} ===============\n".format(self.time)

        	self.botenv.run(5)

		if self.debugGOAP:
        		try:
                		print "GOAP:: Bot's Response: (%s)" % (self.bot.current_response)
        		except UnboundLocalError:
                		pass
        		try:
                		print "GOAP:: Bot's Blackboard: (%s)" % (self.bot.bb.read())
        		except UnboundLocalError:
                		pass
        		try:
                		print "GOAP:: Goals: (%s)" % (self.bot.goals)
        		except UnboundLocalError:
                		pass
        		try:
                		pidx = 0
                		while pidx < len(self.bot.plan):
                        		print "[bot] {} plan has step {} of {}".format(self.bot, pidx, self.bot.plan[pidx])
                        		pidx = pidx + 1
        		except:
                		pass
        	self.time += 1

	def load_commands(self, agent, path):
		import imp
    		mod = imp.load_source("actions", os.path.join(path, "actions.py"))
    		actions = dict([ (c.__name__, c()) for c in mod.exported_actions ])
    		[ agent.add_action(a) for a in actions.values() ]
	def stop(self):
		self.stop_requested = True

class nogoapThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.name = "goapThread"
		self.stop_requested = False
		self.time = 0
	def run(self):
		self.done = True
		self.time = 0

		while not self.stop_requested:
			time.sleep(0.1)
		print "goap thread finished."
	def setDebugGOAP(self, value, sb = "__main"):
		return
	def getGoals(self):
            	return []
	def getGoalsAsStrings(self):
		# return the current list of goals as an array of strings
		goal_strings = [""]
            	return goal_strings
	def addGoals(self, new_goals):
		return
	def removeGoals(self, old_goals):
		return
	def addTagFromPair(self, key, value):
		return
	def get_bot_response(self):
		return ""
	def goap_execute(self):
        	self.time += 1
	def load_commands(self, agent, path):
		return
	def stop(self):
		self.stop_requested = True
		
def parse_args(args):
	"""Parse arguments"""
	global live_aiml_filename, live_gossip_filename, angie_version
	usage = """angie.py [ brain ]"""
	parser = OptionParser(version = "angie.py "+angie_version, usage=usage)
    	# query options
    	parser.add_option("-d", "--debug", default=False,
			action="store_true", help="Add debugging comments to responses")
    	parser.add_option("-v", "--verbose", default=False,
			action="store_true", help="Verbose")
    	parser.add_option("-b", "--brain", default=None,
                      	help="load a brain saved with savebrain")
    	parser.add_option("-B", "--brainstate", default=None,
                      	help="load a brain state saved with savebrainstate")
    	parser.add_option("-l", "--learn", default=None,
                      	help="learn an xml or aiml file")
    	parser.add_option("-s", "--sub", default=None,
                      	help="learn an xml or aiml file for a subbrain")
    	parser.add_option("-m", "--mthresh", default=None,
                      	help="set MoodThreshold (default of 10)")
    	parser.add_option("-n", "--name", default=None,
                      	help="set Name (default of \"Alice\")")
    	parser.add_option("-i", "--input", default=None,
                      	help="submit input text")
    	parser.add_option("-w", "--watchfile", default=None,
                      	help="file to enable debugging output when learning")
    	parser.add_option("-a", "--alttrees", default=True, action="store_true",
                      	help="build brains with Topic-That-Word trees instead of Word-That-Topic (violates AIML spec)")
    	parser.add_option("-t", "--livetemplates", default=None, action="store_true",
                      	help="read dynamic templates from "+ live_aiml_filename)
    	parser.add_option("-g", "--livegossip", default=None, action="store_true",
                      	help="read dynamic gossip from "+ live_gossip_filename)
    	parser.add_option("-p", "--clientport", default="1337",
                      	help="set listening port for client connections (default of 1337)")
    	parser.add_option("-r", "--graphviz", default=None,
                      	help="create a graphviz dot file of the conversation")
    	parser.add_option("", "--dbname", default=None,
                      	help="name of database to use")
    	parser.add_option("", "--testtablename", default=None,
                      	help="name of table to use")
    	parser.add_option("", "--createdatabase", default=False,
			action="store_true", help="Create database if not found")
    	parser.add_option("-o", "--wordnet", default="wordnet.txt",
                      	help="load wordnet definitions from this file")
    	(opts, argsleft) = parser.parse_args(args)

	return opts

def runAIMLTask(taskname, task_args = [], vocalize = False, socket = None):
	nowtime = time.time()
	i = 0
	while i < len(taskCommands):
		if taskname == taskCommands[i]["id"]:
			taskCommands[i]["last_exec_time"] = nowtime
			eThread.executeAIMLTask(taskCommands[i], task_args, vocalize, socket = socket)
		i += 1

def loadWordnetFile(wordnet_filename):
	global iThread

	if not os.path.exists(wordnet_filename):
		return 1
	try:
                fp = open(wordnet_filename, "r")
        except:
                print "Error opening wordnetfile %s for reading." % (wordnetfile)
                return 1

        input = fp.readline()
        while input != "":
		# line format is "dog: 1:def1.1,def1.2; 2:def2.1,def2.2,def2.3"
                input = input.strip()
		wordnet = re.sub("^([^:]+):.*", "\\1", input)
                wordnet_def = []
                defstring = string.split(re.sub("^[^:]+:", "", input), ";")
                for thisdef in defstring:
                	thisdef = thisdef.strip()
                       	thisdefnum = 1
                       	if re.search("^[0-9]+:\S*", thisdef):
                       		thisdefnum = string.atoi(re.sub("^([0-9]+):.*", "\\1", thisdef))
                       		thisdef = re.sub("^[0-9]+:", "", thisdef)
                        if thisdefnum < 1:
                                thisdefnum = 1
                        while thisdefnum > len(wordnet_def):
                                wordnet_def.append( [] )
                        defarray = string.split(thisdef, ",")
                        #print "def %d of %s is (%s)" % (thisdefnum, wordnet, str(defarray))
                        for td in defarray:
                                wordnet_def[thisdefnum - 1].append(td.strip())

                if len(wordnet_def) > 0:
			#print "Need to add definitions for ("+ wordnet +") = ("+ str(wordnet_def) +")."
			retval = iThread.addWordnet(wordnet, wordnet_def)
                #else:
                #        print "No definitions to add."

        	input = fp.readline()

        fp.close()

	return retval

def main(args):
	"""main"""
	global eThread, iThread, vThread, gThread, periodicCommands, taskCommands
	global live_aiml_filename, live_gossip_filename
	global command_path, has_goap

	opts = parse_args(args)
	# The Kernel object is the public interface to
	# the AIML interpreter.
	k = aiml.Kernel()
	k.verbose(False)

	eThread = executionThread()
	eThread.start()
	iThread = interactiveThread(k)
	iThread.start()
	vThread = voiceThread()
	vThread.start()
	if has_goap:
		gThread = goapThread()
	else:
		gThread = nogoapThread()
	gThread.start()
	# Hold off starting the socket thread until after we're done 
	# parsing our arguments in case we get a new port assignment

	if opts.name:
		iThread.setBotName(opts.name)

	if opts.watchfile:
		k.setWatchFile(opts.watchfile)

	if opts.alttrees:
		# The main brain already exists, so make sure it gets
		# the tree selection directly.  The setting on iThread
		# only affects new brains (sub brains).
		print "Using HLPC trees.  This will produce different responses than the AIML standard."
		k.setUseHLPCTrees(opts.alttrees)
		iThread.setUseHLPCTrees(opts.alttrees)

	# load wordnet definitions if there are any.  needs to be done before
	# loading AIML files since they'll be needed for substitutions
	if os.path.exists(opts.wordnet):
		loadWordnetFile(opts.wordnet)

	if opts.brain:
		if os.path.exists(opts.brain):
			iThread.loadBrain(opts.brain)
			iThread.reset_path_on_load = False
			print "Brain loaded from file: "+ opts.brain
		else:
			print "Brain "+ opts.brain +" does not exist."
		if opts.learn:
			print "Ignoring --learn request because --brain was specified"
		if opts.sub:
			print "Ignoring --sub request because --brain was specified"
		if opts.livetemplates:
			print "Ignoring --livetemplates request because --brain was specified"
		if opts.livegossip:
			print "Ignoring --livegossip request because --brain was specified"
		if os.path.exists("angie.bot.values.txt"):
			iThread.loadBotDefaults()
		# name may need to be set again to override what's in the brain
		if opts.name:
			iThread.setBotName(opts.name)
	else:
		if opts.learn:
			if os.path.exists(opts.learn):
				learnstarttime = time.time()
				do_load_aiml_b = False
				if re.search("\.xml$", opts.learn):
					do_load_aiml_b = True
				iThread.learnFile(opts.learn, do_load_aiml_b)
				print "Learned file: "+ opts.learn +" (%.2f seconds)" % (time.time() - learnstarttime)
			else:
				print "File "+ opts.learn +" does not exist."

		if opts.sub:
			sbarray = string.split(opts.sub, ":")
			thissb = sbarray.pop(0)
			sbfile = string.join(sbarray, ":")
			if os.path.exists(sbfile):
				learnstarttime = time.time()
				do_load_aiml_b = False
				if re.search("\.xml$", sbfile):
					do_load_aiml_b = True
				print "Learning subbrain "+ thissb
				iThread.learnFile(sbfile, do_load_aiml_b, sb = thissb)
				print "Subbrain "+ thissb +": Learned file: "+ sbfile +" (%.2f seconds)" % (time.time() - learnstarttime)
			else:
				print "Subbrain "+ thissb +": File "+ sbfile +" does not exist."

		if opts.livetemplates:
			if os.path.exists(live_aiml_filename):
				iThread.learnFile(live_aiml_filename, False, "__main", True, True)
				print "Learned live template file: "+ live_aiml_filename
	
		if opts.livegossip:
			iThread.setGossipFile(live_gossip_filename)
			if os.path.exists(live_gossip_filename):
				iThread.learnGossipFile(live_gossip_filename, False, "__main", True, True)
				print "Learned live gossip file (and will save gossip here): "+ live_gossip_filename
			else:
				print "Live gossip will be saved here: "+ live_gossip_filename

	if opts.brainstate:
		if os.path.exists(opts.brainstate):
			iThread.loadBrainState(opts.brainstate)
			iThread.reset_path_on_load = False
			print "Brain state loaded from file: "+ opts.brainstate
		else:
			print "Brain state "+ opts.brainstate +" does not exist."

	if opts.mthresh:
		iThread.setMoodThreshold(string.atof(opts.mthresh))

	if opts.graphviz:
		iThread.setGraphvizFile(opts.graphviz)
		print "Saving conversation to graphviz dot file: "+ opts.graphviz

	databasename = "__mydb"
	tablename = "__mytable"
	dbh = None
	if opts.dbname:
		databasename = opts.dbname
	if opts.testtablename:
		tablename = opts.testtablename

	try:
		dbh = SnakeSQL.connect(database=databasename, driver='csv', autoCreate=opts.createdatabase)
	except:
		print "Cannot connect to database: "+ databasename
		if not opts.createdatabase:
			print "Rerun with --createdatabase option to create it."
		if eThread.isAlive():
			eThread.stop()
			while eThread.isAlive():
				time.sleep(0.05)
		if vThread.isAlive():
			vThread.stop()
			while vThread.isAlive():
				time.sleep(0.05)
		if gThread.isAlive():
			gThread.stop()
			while gThread.isAlive():
				time.sleep(0.05)
		if iThread.isAlive():
			iThread.stop_requested = True
		sys.exit()
		
	# pass the cursor on to iThread and the AIML kernel for use in _processGetdb()
	iThread.setDBConnection(dbh)

	dbtest = False
	if dbtest:
		print "database name: "+ databasename
		varcolumn = "weight"
		vartable = "__mytable"
		varname = "whale"
		sth = dbh.cursor()
		results = []
		try:
        	        print "select "+ varcolumn +" from "+ vartable +" where rowkey = '"+ varname +"'"
        	        sth.execute("select "+ varcolumn +" from "+ vartable +" where rowkey = '"+ varname +"'")
			results = sth.fetchall()
			print "there are %d rows" % (len(results))
			print "database rows before insert:"
			print "result:: ("+ results[0][0] +")"
			for row in results:
				print "result: ("+ row[0] +")"
		except:
			print "No rows in "+ tablename +", creating:"
			sth.execute("create table "+ tablename +" (rowkey String primary key, weight String, height String, volume String)")
		print "Inserting new rows:"
		sth.execute("insert into "+ tablename +" (rowkey,weight,height,volume) values (?, ?, ?, ?)", ["frog", "1", "", "0.5"] )
		dbh.commit()
		if eThread.isAlive():
			eThread.stop()
			while eThread.isAlive():
				time.sleep(0.05)
		if vThread.isAlive():
			vThread.stop()
			while vThread.isAlive():
				time.sleep(0.05)
		if gThread.isAlive():
			gThread.stop()
			while gThread.isAlive():
				time.sleep(0.05)
		if iThread.isAlive():
			iThread.stop_requested = True
		sys.exit()
	
	iThread.startProcessingInput()

	# Start socket thread now, possibly with a new port
	sThread = socketThread()
	sThread.setport(string.atoi(opts.clientport))
	sThread.start()

	# Setup some periodic commands to be fed into iThread, later
	# to be migrated into kuThread
	pcommand = {}
	pcommand["id"] = 0
	pcommand["last_exec_time"] = 0
	pcommand["period_seconds"] = 60
	pcommand["command"] = command_path +"/whatwifi.pl"
	pcommand["command_args"] = ["-d"]
	#periodicCommands.append(pcommand)

	# Setup some task commands that can be called by responses
	# generated by iThread.  Maybe migrate to kuThread, maybe
	# keep in main()
	tcommand = {}
	tcommand["id"] = "listwifi"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/whatwifi.pl"
	tcommand["command_args"] = ["-d"]
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "listwifisamples"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/wifisamplelist.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "whereami"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think><set name=\"location\">"
	tcommand["aiml_footer"] = "</set>"
	tcommand["command"] = command_path +"/whereami.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "savelocation"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think><set name=\"location\">"
	tcommand["aiml_footer"] = "</set>"
	tcommand["command"] = command_path +"/savelocation.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "updatelocation"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/wifi-location.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "batterylife"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/batteryinfo.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "traineta"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/traininfo.pl"
	tcommand["command_args"] = ["-eta"]
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "traininfo"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/traininfo.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "weather"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think><set name=\"topic\">TASKLIST</set></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/weather.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "spell"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "The correct spelling is: "
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/how.to.spell.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)
	tcommand = {}
	tcommand["id"] = "quietspell"
	tcommand["last_exec_time"] = 0
	tcommand["aiml_header"] = "<think></think>"
	tcommand["aiml_footer"] = ""
	tcommand["command"] = command_path +"/how.to.spell.pl"
	tcommand["command_args"] = []
	taskCommands.append(tcommand)

	vThread.say("angie.py started")

	if opts.input:
		iThread.process_input_and_output(opts.input)
		iThread.process_input_and_output("quit")

	# Wait for interactive thread to finish, kick off periodic
	# commands via execution thread, and polling execution thread
	# for any results the interactive thread might need
	pcommand_time = time.time() - 0.5
	while iThread.isAlive():
		time.sleep(0.1)
		nowtime = time.time()

		# Execute any periodic commands that are ready to run
		i = 0
		while i < len(periodicCommands):
			if (nowtime - periodicCommands[i]["last_exec_time"]) > periodicCommands[i]["period_seconds"]:
				periodicCommands[i]["last_exec_time"] = nowtime
				eThread.execute(periodicCommands[i]["command"], periodicCommands[i]["command_args"])
			i += 1
				
		# Only poll for periodic command results every 1/2 second
		if nowtime - pcommand_time >= 0.5:
			while eThread.isAlive() and eThread.has_results():
				(output, vocalize, socket) = eThread.get_next_result()
				iThread.new_async_response(output, vocalize, socket)
			pcommand_time = time.time()

		# Poll for task command results every time
		while eThread.isAlive() and eThread.has_aiml_results():
			(aiml_text, vocalize, socket) = eThread.get_next_aiml()
			iThread.new_aiml(aiml_text, vocalize, socket)

	if sThread.isAlive():
		sThread.stop()
		while sThread.isAlive():
			time.sleep(0.05)

	if eThread.isAlive():
		eThread.stop()
		while eThread.isAlive():
			time.sleep(0.05)
	
	if vThread.isAlive():
		vThread.stop()
		while vThread.isAlive():
			time.sleep(0.05)

	if gThread.isAlive():
		gThread.stop()
		while gThread.isAlive():
			time.sleep(0.05)

	
	print "Done."

if __name__ == "__main__":
	main(sys.argv[1:])
