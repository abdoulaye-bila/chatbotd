#!/usr/bin/python
###################################################################
# Copyright (C) BILA
###################################################################

import os, sys, time
from optparse import OptionParser
import string, re
import socket

def parse_args(args):
        """Parse arguments"""

        usage = """client.bridge.py """
        parser = OptionParser(version = "client.bridge.py 0.1", usage=usage)
        # query options
        parser.add_option("-v", "--verbose", default=False,
                        action="store_true", help="Verbose")
        parser.add_option("-d", "--debug", default=False,
                        action="store_true", help="only print what would have been sent")
        parser.add_option("-s", "--start", default="hello.",
                        help="sentence to start off sending to the 'left' server to initiate conversation (default: \"hello.\")")
        parser.add_option("-p", "--prod", default="do you want to talk about something else?",
                        help="sentence to send to a bot that has stopped responding in order to get the conversation moving again (default: \"what do you want to talk about?\")")
        parser.add_option("-l", "--leftport", default="1337",
                        help="port where the 'left' server is listening (default 1337)")
        parser.add_option("-r", "--rightport", default="1338",
                        help="port where the 'right' server is listening (default 1338)")
        parser.add_option("-n", "--numrounds", default="10",
                        help="number of rounds to let the conversation run for before aborting (default 10)")
        (opts, argsleft) = parser.parse_args(args)

        return opts

def cleaninput(input):
	input = input.strip()
	input = re.sub("^Topic: [^\n]*\n", "", input)
	input = re.sub("^GOAP response:: ", "", input)
	input = re.sub("^Saving empty response state for: .*", "", input)
	return input

def main(args):
	"""main"""

	opts = parse_args(args)

	leftsock = socket.socket()
	leftsock.connect(("127.0.0.1", string.atoi(opts.leftport)))
	leftsock.setblocking(0)
	print "Resetting left state..."
	leftsock.sendall("forget everything silently\n")
	time.sleep(1.5)
	idata = ""
	try:
		idata = leftsock.recv(1024)
		idata = cleaninput(idata)
	except:
		pass
	if idata != "":
		idata = idata.strip()
		print "Initial leftdata: ("+ idata +")"

	rightsock = socket.socket()
	rightsock.connect(("127.0.0.1", string.atoi(opts.rightport)))
	rightsock.setblocking(0)
	print "Resetting right state..."
	rightsock.sendall("forget everything silently\n")
	time.sleep(1.5)
	idata = ""
	try:
		idata = rightsock.recv(1024)
		idata = cleaninput(idata)
	except:
		pass
	if idata != "":
		idata = idata.strip()
		print "Initial rightdata: ("+ idata +")"

	roundnum = 0
	maxrounds = string.atoi(opts.numrounds)
	print "Sending left: ("+ opts.start +")"
	leftsock.sendall(opts.start+"\n")
	time.sleep(1.0)
	while roundnum < maxrounds:
		time.sleep(0.5)
		roundnum += 1
		idata = ""
		try:
			idata = leftsock.recv(1024)
			idata = cleaninput(idata)
		except:
			pass
		if idata != "":
			idata = idata.strip()
			print "left says: ("+ idata +")"
			rightsock.sendall(idata +"\n")
			time.sleep(1.0)
			idata = ""
			try:
				idata = rightsock.recv(1024)
				idata = cleaninput(idata)
			except:
				pass
			if idata == "":
				# Maybe they need a prod
				print "right has nothing to say.";
				if opts.prod != "":
					print "prodding right with: ("+ opts.prod +")"
					rightsock.sendall(opts.prod+"\n")
					time.sleep(1.0)
					idata = ""
					try:
						idata = rightsock.recv(1024)
						idata = cleaninput(idata)
					except:
						pass
			if idata != "":
				idata = idata.strip()
				print "right says: ("+ idata +")"
				leftsock.sendall(idata +"\n")
				time.sleep(1.0)
		else:
			# Maybe they need a prod
			print "left has nothing to say.";
			if opts.prod != "":
				print "prodding left with: ("+ opts.prod +")"
				leftsock.sendall(opts.prod+"\n")
				time.sleep(1.0)
	print "done"
	leftsock.close()
	rightsock.close()

if __name__ == "__main__":
        main(sys.argv[1:])

