#!/usr/bin/python
###################################################################
# Copyright (C) BILA

###################################################################

import os, sys, time
from threading import Thread
import copy
from optparse import OptionParser
import string
import re
import random

sys.path.append("~/PyAIML-0.8.5")
import aiml

class interactiveThread(Thread):
	def __init__(self, k):
		Thread.__init__(self)
		self.brains = {}
		self.brains["__main"] = k
		self.brains["__main"].verbose(False)
		self.name = "interactiveThread"
		self.useHLPCtrees = False
		self.startup_mode = True
		self.stop_requested = False
		self.async_response_queue = []
		self.async_vocalize_queue = []
		self.async_socket_queue = []
		self.aiml_queue = []
		self.aiml_vocalize_queue = []
		self.aiml_socket_queue = []
		self.moods = {}
		self.mood_threshold = 10
		self.current_mood = "__main"
		self.vocalize = False
		self.showSrais = False
		self.showMatches = False
		self.temp_vocalize = False
		self.empty_response_filename = "empty.responses.txt"
		self.bad_response_filename = "bad.responses.txt"
		self.botvalues = {}
		self.botvalues["name"] = "Alice"
		self.botvalues["gender"] = "female"
		self.botvalues["hair_color"] = "brown"
		self.botvalues["eye_color"] = "brown"
		self.botvalues["height"] = "5' 6\""
		self.botvalues["master"] = "Mud"
		self.botvalues["master_gender"] = "male"
		self.botvalues["location"] = "New York"
		self.botvalues["birthplace"] = "Dallas"
		self.botvalues["birthday"] = "July 4"
		self.normalvalues = {}
		self.setBotName(self.botvalues["name"])
		self.setBotMaster(self.botvalues["master"])
		self.processBotValues("__main")
		self.processNormalValues("__main")
		#self.last_seen = {}
		self.templatemode = False
		self.drafttemplate = {}
		self.drafttemplate["pattern"] = ""
		self.drafttemplate["template"] = []
		self.live_templates = {}
		self.dynpatterns = {}
		self.gossip_file = ""
		self.dbh = None
		self.recent_added_categories = {}
		self.recent_skipped_categories = {}
		self.added_categories = {}
		self.skipped_categories = {}
		self.empty_response = False
	def run(self):
		# Loop, reading user input from the command
		# line and printing responses.
		sb = "__main"
		self.processBotValues(sb)
		self.processNormalValues(sb)
		# wait until we're told to start processing input
		while not self.stop_requested and self.startup_mode:
			time.sleep(0.1)
		while not self.stop_requested:
			input = string.strip(raw_input("> "))
			if input == "":
				continue
			self.process_input_and_output_with_mood(input, sb = sb)
		if len(self.live_templates) > 0:
			print "saving live templates."
			self.saveLiveTemplates()
		if self.gossip_file != "":
			didsave = False
			for sb in self.brains:
				if self.brains[sb].saveGossip():
					didsave = True
			if didsave:
				print "saved new gossip templates."
		print "interactive thread finished."

	def startProcessingInput(self):
		self.startup_mode = False

	def process_input_and_output_with_mood(self, input, sb = "__main", socket = None):
		new_mood = self.chooseMood()
		if new_mood != self.current_mood:
			msg = "Changing mood from ("+ self.current_mood +") to ("+ new_mood +")..."
			print msg
		self.current_mood = new_mood
		# The current brain and the main need to know what
		# the mood state is before they process input.
		self.saveMoodsToBrain(sb = self.current_mood)
		if sb != "__main":
			self.saveMoodsToBrain(sb = "__main")
		resp = self.process_input(input, sb = self.current_mood, socket = socket)
		self.process_output(resp, sb = self.current_mood, socket = socket)
		self.decayMoods()
		for mood in self.moods:
			if self.moods[mood] > 0:
				msg = "Mood: "+ mood +" = "+ str(self.moods[mood])
				print msg
		if not self.empty_response:
			print "Found pattern matching: ("+ input +") in existing pattern: ("+ self.brains[sb]._brain._lastPattern +") on line ("+ str(self.brains[sb]._brain._lastPatternLine) +") of file ("+ self.brains[sb]._brain._lastPatternFile +")"
	def saveLiveTemplates(self):
		global live_aiml_filename
		if len(self.live_templates) <= 0:
			return
		try:
			fp = open(live_aiml_filename, "w")
			fp.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
			fp.write("<aiml version=\"1.0\">\n")
			fp.write("<meta name=\"language\" content=\"en\"/>\n")
			fp.write("\n")
			# These are the templates that have been
			# added since we began running.
			for key,tem in self.live_templates.items():
				(pattern,that,topic,restriction) = key
				(restrictedA, restrictedB, restrictedOp) = restriction
				isRestricted = False
				if restrictedA != "" or restrictedB != "" or restrictedOp != "":
					isRestricted = True
				if isRestricted:
					fp.write("<restricted a=\""+ restrictedA +"\" b=\""+ restrictedB +"\" op=\""+ restrictedOp +"\">\n")
				if topic != "" and topic != "*":
					fp.write("<topic name=\""+ topic +"\">\n")
				fp.write("<category>\n")
				fp.write("<pattern>"+ pattern +"</pattern>\n")
				if that != "" and that != "*":
					fp.write("<that>"+ that +"</that>\n")
				fp.write(re.sub("&","&amp;", self.brains["__main"]._elementToText(tem)))
				fp.write("\n")
				fp.write("</category>\n")
				if topic != "" and topic != "*":
					fp.write("</topic>\n")
				if isRestricted:
					fp.write("</restricted>\n")
				fp.write("\n")
			# These are the ones that were in the file
			# when we were first started up.
			for key,tem in self.dynpatterns.items():
				(pattern,that,topic,restriction) = key
				(restrictedA, restrictedB, restrictedOp) = restriction
				isRestricted = False
				if restrictedA != "" or restrictedB != "" or restrictedOp != "":
					isRestricted = True
				if isRestricted:
					fp.write("<restricted a=\""+ restrictedA +"\" b=\""+ restrictedB +"\" op=\""+ restrictedOp +"\">\n")
				if topic != "" and topic != "*":
					fp.write("<topic name=\""+ topic +"\">\n")
				fp.write("<category>\n")
				fp.write("<pattern>"+ pattern +"</pattern>\n")
				if that != "" and that != "*":
					fp.write("<that>"+ that +"</that>\n")
				fp.write(re.sub("&","&amp;", self.brains["__main"]._elementToText(tem)))
				fp.write("\n")
				fp.write("</category>\n")
				if topic != "" and topic != "*":
					fp.write("</topic>\n")
				if isRestricted:
					fp.write("</restricted>\n")
				fp.write("\n")

			fp.write("</aiml>\n")
			fp.close()
			print "Created live template aiml file: "+ live_aiml_filename
		except Exception, msg:
			print "Error creating live template aiml file: "+ live_aiml_filename
			print "Error is: "+ str(msg)

	def setDBConnection(self, _dbh):
		self.dbh = _dbh
		for sb in self.brains:
			self.brains[sb].setDBConnection(self.dbh)
	def setUseHLPCTrees(self, value):
		self.useHLPCtrees = value
	def getUseHLPCTrees(self):
		return self.useHLPCtrees
	def process_input_and_output(self, input):
		resp = self.process_input(input, sb = self.current_mood)
		self.process_output(resp, sb = self.current_mood)
		if not self.empty_response:
			print "Found pattern matching: ("+ input +") in existing pattern: ("+ self.brains[self.current_mood]._brain._lastPattern +") on line ("+ str(self.brains[self.current_mood]._brain._lastPatternLine) +") of file ("+ self.brains[self.current_mood]._brain._lastPatternFile +")"
	def chooseMood(self):
		next_mood = "__main"
		next_mood_value = self.mood_threshold
		for mood in self.moods:
			if self.moods[mood] > next_mood_value:
				next_mood = mood
				next_mood_value = self.moods[mood]
		return next_mood
	def process_output(self, output, sb = "__main", socket = None):
		self.empty_response = False
		vocalize = False
		if self.temp_vocalize or self.vocalize:
			vocalize = True
		self.temp_vocalize = False
		if output != "":
			topic = self.brains[sb].getPredicate("topic")
			if topic != "":
				msg = "Topic: %s" % (topic)
				print msg
			print output
			## FIXME: EXEC_TASK responses don't vocalize.
			##        Need to pass an argument that makes it
			##        all the way back here..
			##        Or rather print_async_response_queue()
		elif sb != "__main":
			## subbrains get penalized for having no
			## response (in addition to normal decay cycle)...
			self.decayMood(sb)
		# Track info about empty responses, but not when we're
		# quitting or were just fed a secrettasktag
		if output == "" and not self.stop_requested and self.brains[sb].getInput(index = 1) != "secrettasktag":
			self.empty_response = True
			msg = "Empty response for: "+ self.brains[sb].getInput(index = 1)
			print msg
	def setMoodThreshold(self, value):
		self.mood_threshold = value
	def increaseMood(self, mood, value = 1):
		if not mood in self.moods:
			self.moods[mood] = 0
		self.moods[mood] = self.moods[mood] + value
	def decreaseMood(self, mood, value = 1):
		if not mood in self.moods:
			self.moods[mood] = 0
		self.moods[mood] = self.moods[mood] - value
		if self.moods[mood] < 0:
			self.moods[mood] = 0
	def getMood(self, mood):
		if not mood in self.moods:
			return 0
		return self.moods[mood]
	def saveMoodsToBrain(self, sb = "__main"):
		for mood in self.moods:
			self.setValue("bot_mood_"+mood, self.moods[mood], sb = sb)
	def decayMoods(self):
		for mood in self.moods:
			self.decayMood(mood)
	def decayMood(self, mood):
		if mood in self.moods:
			self.decreaseMood(mood, value=0.25)
	def process_input(self, input, sb = "__main", socket = None):
		input = string.strip(input)
		# Change two or two periods at the end to a single
		input = re.sub("\.\.+$",".", input)
		# And change three or more periods in the middle to a single
		input = re.sub("\.\.\.+",".", input)
		resp = ""
		if input == "quit":
			self.stop_requested = True
		elif re.search("^topic: .*", input) and self.templatemode:
			input = string.upper(re.sub("^topic: ", "", input)).strip()
			if input != "" and input != "*":
				self.drafttemplate["topic"] = input
				resp = "set topic to: "+ self.drafttemplate["topic"]
		elif re.search("^that: .*", input) and self.templatemode:
			input = string.upper(re.sub("^that: ", "", input)).strip()
			if input != "" and input != "*":
				self.drafttemplate["that"] = input
				resp = "set that to: "+ self.drafttemplate["that"]
		elif (re.search("^end template.*", input) or input == ".") and self.templatemode:
			print "This script does not handle templates created on the fly."
			self.templatemode = False
			self.drafttemplate = {}
			self.drafttemplate["pattern"] = ""
			self.drafttemplate["template"] = []
			resp = "added new template."
		elif self.templatemode:
			self.drafttemplate["template"].append(input)
			# save the text as part of the new template...
			resp = "template mode"
		elif re.search("^start template .*", input) or re.search("^add template .*", input):
			username = self.getValue("name", sb = sb)
			botmaster = self.getBotValue("master", sb = sb)
			if username.lower() == botmaster.lower():
				if re.search("^start template .*", input):
					input = string.upper(re.sub("^start template ", "", input))
				else:
					input = string.upper(re.sub("^add template ", "", input))
				input.strip()
				if input != "" and input != "*":
					self.templatemode = True
					self.drafttemplate = {}
					self.drafttemplate["pattern"] = input
					self.drafttemplate["template"] = []
					resp = "enter contents of new template. start a line with \"topic: \" or \"that: \" to set <topic> or <that>. end with \"end template\" or \".\". all other lines are the response body (which can include aiml markup)."
				else:
					resp = "Usage: start template PATTERN"
			else:
				resp = "That functionality is not available for regular users."
		elif re.search("^!\S*", input):
			text = re.sub("^!", "", input)
			os.system(text)
			resp = "Exec'd command \""+ text +"\"."
		elif re.search("^echob .*", input) or re.search("^echo bot .*", input):
			input = re.sub("^echo bot ", "echob ", input)
			key = re.sub("^echob ", "", input)
			value = self.getBotValue(key)
			resp = "bot "+ key +" = ("+ value +")"
		elif re.search("^echo .*", input):
			key = re.sub("^echo ", "", input)
			if key == "that":
				value = self.brains[sb].getThat()
			elif key == "input":
				value = self.brains[sb].getInput()
			elif key == "mthresh":
				value = str(self.mood_threshold)
			elif key == "showsrais":
				value = str(self.showSrais)
			elif key == "showmatches":
				value = str(self.showMatches)
			else:
				value = self.getValue(key)
			resp = key +" = ("+ value +")"
		elif re.search("^vartime .*", input):
			key = re.sub("^vartime ", "", input)
			resp = key +" = ("+ str(self.getValueSetTime(key)) +")"
		elif re.search("^setlastseen .*", input):
			key = re.sub("^setlastseen ", "", input)
			self.saveLastTimeThisName(key)
			resp = "setlastseen ("+ key +"): ("+ str(self.getLastTimeThisName(key)) +")"
		elif re.search("^lastseen .*", input):
			key = re.sub("^lastseen ", "", input)
			resp = "lastseen ("+ key +"): ("+ str(self.getLastTimeThisName(key)) +")"
		elif re.search("^previousseen .*", input):
			key = re.sub("^previousseen ", "", input)
			resp = "previousseen ("+ key +"): ("+ str(self.getPreviousTimeThisName(key)) +")"
		elif re.search("^setb \S* = .*", input) or re.search("^set bot \S* = .*", input):
			input = re.sub("^set bot ", "setb ", input)
			key = re.sub("^setb *", "", input)
			key = re.sub(" = .*", "", key)
			value = re.sub("^setb \S* = ", "", input)
			# A value of "" doesn't mean two quotes, it
			# means empty string.
			if value == "\"\"":
				value = ""
			self.setBotValue(key, value)
			resp = "set bot "+ key +" = ("+ value +")"
		elif re.search("^set \S* = .*", input):
			key = re.sub("^set *", "", input)
			key = re.sub(" = .*", "", key)
			value = re.sub("^set \S* = ", "", input)
			# A value of "" doesn't mean two quotes, it
			# means empty string.
			if value == "\"\"":
				value = ""
			if key == "mthresh":
				self.setMoodThreshold(string.atof(value))
			elif key == "vocal" or key == "vocalize":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setVocalize(bvalue)
			elif key == "showsrais":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowSrais(bvalue)
			elif key == "showmatches":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowMatches(bvalue)
			elif key == "debugpath":
				bvalue = False
				if string.lower(value) == "true" or string.lower(value) == "t":
					bvalue = True
					value = "True"
				else:
					value = "False"
				self.setShowMatches(bvalue)
				self.setShowSrais(bvalue)
			else:
				self.setValue(key, value)
			resp = "set "+ key +" = ("+ value +")"
		elif re.search("^debugfile .*", input):
			value = re.sub("^debugfile ", "", input)
			value = re.sub("^= ", "", value)
			if value == "nil" or value == "null":
				value = ""
			self.brains[sb].setWatchFile(value)
			resp = "debugfile = ("+ value +")"
		elif re.search("^savebrain \S*", input):
			brainname = re.sub("^savebrain ", "", input)
			if self.saveBrain(brainname):
				resp = "Error saving brain to file: "+ brainname
			else:
				resp = "Brain saved to file: "+ brainname
		elif re.search("^loadbrain \S*", input):
			brainname = re.sub("^loadbrain ", "", input)
			if self.loadBrain(brainname):
				resp = "Error loading brain "+ brainname
			else:
				resp = "Brain loaded from file: "+ brainname
		elif re.search("^learn .*", input) or re.search("^debuglearn .*", input):
			do_debug = False
			if re.search("^debuglearn .*", input):
				do_debug = True
				input = re.sub("^debug", "", input)
			learnfile = re.sub("^learn ", "", input)
			retval = 1
			do_load_aiml_b = True
			# if the learnfile begins with a "+" or does NOT
			# end in ".xml", don't do load_aiml_b....
			if re.search("^\+", learnfile) or not re.search("\.xml$", learnfile):
				learnfile = re.sub("^\+", "", learnfile)
				do_load_aiml_b = False
			resp = ""
			if do_debug:
				self.brains[sb].setWatchFile(learnfile)
				resp = "debugfile = ("+ learnfile +")\n"
			retval = self.learnSkippingConflicts(learnfile, do_load_aiml_b)
			if retval:
				resp += "Error loading learn file "+ learnfile
			else:
				resp += "learned file: "+ learnfile
		elif re.search("^exec \S*", input):
			cmd_args = string.split(re.sub("^exec ", "", input), " ")
			cmd = cmd_args.pop(0)
			print "We don't do executions in this code."
		elif input == "check coding standards":
			for key,tem in self.recent_added_categories.items():
				self.check_coding_standards(key, tem)
			for key,tem in self.recent_skipped_categories.items():
				self.check_coding_standards(key, tem)
		else:
			self.temp_vocalize = False
			if re.search("^vocal: .*", input):
				input = re.sub("^vocal: ", "", input)
				self.temp_vocalize = True
			last_name_state = self.getValue("name", sb = sb)
			## If we have a name, remember we're talking
			## to them.  Don't force the save, though.
			if last_name_state != "":
				self.saveLastTimeThisName(last_name_state)
			resp = self.brains[sb].respond(input)
			if last_name_state != self.getValue("name", sb = sb):
				## Name has changed, force save of the current
				## name and the new name.
				self.saveLastTimeThisName(last_name_state, True)
				self.saveLastTimeThisName(self.getValue("name", sb = sb), True)
			## Get predicate mood_plus, if it has a value
			## increase that value's counter.
			## Check mood_minus as well, decreasing its value
			## mood_rplus, mood_rminus add or remove 0 or 1
			## at random.  mood_rmod is random -1, 0, or 1
			## Then set these to an empty string.
			mood = self.getValue("mood_plus", sb = sb)
			if mood != "":
				resp = "Increasing mood: "+ mood +"\n"+ resp
				self.increaseMood(mood)
			mood = self.getValue("mood_minus", sb = sb)
			if mood != "":
				resp = "Decreasing mood: "+ mood +"\n"+ resp
				self.decreaseMood(mood)
			mood = self.getValue("mood_rplus", sb = sb)
			if mood != "":
				rint = random.randint(0, 1)
				if rint > 0:
					resp = "Increasing mood: "+ mood +"\n"+ resp
					self.increaseMood(mood)
			mood = self.getValue("mood_rminus", sb = sb)
			if mood != "":
				rint = random.randint(0, 1)
				if rint > 0:
					resp = "Decreasing mood: "+ mood +"\n"+ resp
					self.decreaseMood(mood)
			mood = self.getValue("mood_rmod", sb = sb)
			if mood != "":
				rint = random.randint(-1, 1)
				if rint > 0:
					resp = "Increasing mood: "+ mood +"\n"+ resp
					self.increaseMood(mood)
				elif rint < 0:
					resp = "Decreasing mood: "+ mood +"\n"+ resp
					self.decreaseMood(mood)
			self.setValue("mood_plus", "")
			self.setValue("mood_minus", "")
			self.setValue("mood_rplus", "")
			self.setValue("mood_rminus", "")
			self.setValue("mood_rmod", "")
		if re.search("^::EXEC_TASK::", resp):
			print "Resp has exec %s" % resp
			task = re.sub("^::EXEC_TASK::", "", resp)
			task_args = []
			if re.search("::", task):
				task_args = string.split(re.sub("^[^:]+::", "", task),"::")
				task = re.sub("::.*", "", task)
			resp = "Executing task: "+ task +" "+ string.join(task_args, ", ")
			runAIMLTask(task, task_args, self.temp_vocalize, socket = socket)
		return resp
	def check_coding_standards(self, key, tem):
		(thispattern, thisthat, thistopic, thisrestriction) = key
		# FIXME: add more checks
		if re.search("DO YOU NOT", thispattern):
			print "Found \"DO YOU NOT\" pattern ("+ thispattern +") should probably be ("+ re.sub("DO YOU NOT", "DO NOT YOU", thispattern) +")"
		if re.search("WILL YOU NOT", thispattern):
			print "Found \"WILL YOU NOT\" pattern ("+ thispattern +") should probably be ("+ re.sub("WILL YOU NOT", "WILL NOT YOU", thispattern) +")"
		if re.search("WILL NEVER", thispattern):
			print "Found \"WILL NEVER\" pattern ("+ thispattern +") should probably be ("+ re.sub("WILL NEVER", "WILL NOT EVER", thispattern) +")"
		if re.search("ALOT", thispattern):
			print "Found \"ALOT\" pattern ("+ thispattern +") should probably be ("+ re.sub("ALOT", "A LOT", thispattern) +")"
	def processBotValues(self, sb = "__main"):
		for key in self.botvalues:
			self.setBotValue(key, self.botvalues[key], sb = sb)
	def processNormalValues(self, sb = "__main"):
		for key in self.normalvalues:
			self.setValue(key, self.normalvalues[key], sb = sb)
	def getBotName(self, sb = "__main"):
		return self.botvalues["name"]
	def setBotName(self, value, sb = "__main"):
		self.botvalues["name"] = value
		self.setBotValue("name", value, sb)
	def getBotMaster(self, sb = "__main"):
		return self.botvalues["master"]
	def setBotMaster(self, value, sb = "__main"):
		self.botvalues["master"] = value
		self.setBotValue("master", value, sb)
	def setValue(self, key, value, sb = "__main"):
		self.brains[sb].setPredicate(key, str(value))
	def getValue(self, key, sb = "__main"):
		return self.brains[sb].getPredicate(key)
	def getValueSetTime(self, key, sb = "__main"):
		return self.brains[sb].getPredicateSetTime(key)
	def setBotValue(self, key, value, sb = "__main"):
		self.brains[sb].setBotPredicate(key, str(value))
	def getBotValue(self, key, sb = "__main"):
		return self.brains[sb].getBotPredicate(key)
	def learnUnderscoresOnlyFromFile(self, learnfile, do_load_aiml_b = True, sb = "__main", verbose = True, savePatterns = False):
		if not sb in self.brains:
			self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(False)
			self.brains[sb].setUseHLPCTrees(self.useHLPCtrees)
		oldverbose = self.brains[sb].verbose()
		# FIXME: At least for debugging, check in the aiml/hlpc
		# FIXME: directory if there's no file found.
		# FIXME: And why not append ".aiml" too.
		if not os.path.exists(learnfile):
			if os.path.exists("aiml/hlpc/"+ learnfile):
				learnfile = "aiml/hlpc/"+ learnfile
			elif os.path.exists(learnfile +".aiml"):
				learnfile = learnfile +".aiml"
			elif os.path.exists("aiml/hlpc/"+ learnfile +".aiml"):
				learnfile = "aiml/hlpc/"+ learnfile +".aiml"
		if os.path.exists(learnfile):
			if verbose == True:
				self.brains[sb].verbose(True)
			self.brains[sb].addUnderscoreOnly = True
			(added_patterns, skipped_patterns) = self.brains[sb].learnUnderscoreOnly(learnfile)
			if do_load_aiml_b:
				# Check to see if "load aiml b" is a skipped pattern and if so, add it normally
				for key,tem in skipped_patterns.items():
					(thispattern, thisthat, thistopic, thisrestriction) = key
					if thispattern == "LOAD AIML B":
                        			print "Found LOAD AIML B key: ("+ str(key) +")"
						self.brains[sb]._brain.add(key,tem)
				self.brains[sb].respond("load aiml b")
			# remember all the added categories
			self.recent_added_categories = {}
			for key,tem in self.brains[sb].added_categories.items():
				(thispattern, thisthat, thistopic, thisrestriction) = key
				if not do_load_aiml_b or thispattern != "LOAD AIML B":
					self.recent_added_categories[key] = copy.deepcopy(tem)
					self.added_categories[key] = copy.deepcopy(tem)
			# remember all the skipped categories (load aiml b is not skipped if we've set do_load_aiml_b...)
			self.recent_skipped_categories = {}
			for key,tem in self.brains[sb].skipped_categories.items():
				(thispattern, thisthat, thistopic, thisrestriction) = key
				if not do_load_aiml_b or thispattern != "LOAD AIML B":
					self.recent_skipped_categories[key] = copy.deepcopy(tem)
					self.skipped_categories[key] = copy.deepcopy(tem)
			self.brains[sb].verbose(oldverbose)
			return 0
		else:
			return 1
	def learnSkippingConflicts(self, learnfile, do_load_aiml_b = True, sb = "__main", verbose = False, savePatterns = False):
		if not sb in self.brains:
			self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(False)
			self.brains[sb].setUseHLPCTrees(self.useHLPCtrees)
		oldverbose = self.brains[sb].verbose()
		# FIXME: At least for debugging, check in the aiml/hlpc
		# FIXME: directory if there's no file found.
		# FIXME: And why not append ".aiml" too.
		if not os.path.exists(learnfile):
			if os.path.exists("aiml/hlpc/"+ learnfile):
				learnfile = "aiml/hlpc/"+ learnfile
			elif os.path.exists(learnfile +".aiml"):
				learnfile = learnfile +".aiml"
			elif os.path.exists("aiml/hlpc/"+ learnfile +".aiml"):
				learnfile = "aiml/hlpc/"+ learnfile +".aiml"
		if os.path.exists(learnfile):
			if verbose == True:
				self.brains[sb].verbose(True)
			else:
				self.brains[sb].verbose(False)
			self.brains[sb].skipConflicts = True
			(added_patterns, skipped_patterns) = self.brains[sb].learnSkippingConflicts(learnfile)
			if do_load_aiml_b:
				# Check to see if "load aiml b" is a skipped pattern and if so, add it normally
				for key,tem in skipped_patterns.items():
					(thispattern, thisthat, thistopic, thisrestriction) = key
					if thispattern == "LOAD AIML B":
                        			print "Found LOAD AIML B key: ("+ str(key) +")"
						self.brains[sb]._brain.add(key,tem)
				self.brains[sb].respond("load aiml b")
			# remember all the added categories
			self.recent_added_categories = {}
			for key,tem in self.brains[sb].added_categories.items():
				(thispattern, thisthat, thistopic, thisrestriction) = key
				if not do_load_aiml_b or thispattern != "LOAD AIML B":
					self.recent_added_categories[key] = copy.deepcopy(tem)
					self.added_categories[key] = copy.deepcopy(tem)
			# remember all the skipped categories (load aiml b is not skipped if we've set do_load_aiml_b...)
			self.recent_skipped_categories = {}
			for key,tem in self.brains[sb].skipped_categories.items():
				(thispattern, thisthat, thistopic, thisrestriction) = key
				if not do_load_aiml_b or thispattern != "LOAD AIML B":
					self.recent_skipped_categories[key] = copy.deepcopy(tem)
					self.skipped_categories[key] = copy.deepcopy(tem)
			self.brains[sb].verbose(oldverbose)
			return 0
		else:
			return 1
	def learnGossipFile(self, learnfile, do_load_aiml_b = True, sb = "__main", verbose = True, saveGossipPatterns = False):
		if not sb in self.brains:
			self.brains[sb] = aiml.Kernel()
			self.brains[sb].verbose(False)
			self.brains[sb].setUseHLPCTrees(self.useHLPCtrees)
		oldverbose = self.brains[sb].verbose()
		# FIXME: At least for debugging, check in the aiml/hlpc
		# FIXME: directory if there's no file found.
		# FIXME: And why not append ".aiml" too.
		if not os.path.exists(learnfile):
			if os.path.exists("aiml/hlpc/"+ learnfile):
				learnfile = "aiml/hlpc/"+ learnfile
			elif os.path.exists(learnfile +".aiml"):
				learnfile = learnfile +".aiml"
			elif os.path.exists("aiml/hlpc/"+ learnfile +".aiml"):
				learnfile = "aiml/hlpc/"+ learnfile +".aiml"
		if os.path.exists(learnfile):
			self.setGossipFile(learnfile, sb)
			if verbose == True:
				self.brains[sb].verbose(True)
			self.brains[sb].loadGossip(saveGossipPatterns)
			if do_load_aiml_b:
				self.brains[sb].respond("load aiml b")
			self.brains[sb].verbose(oldverbose)
			return 0
		else:
			return 1
	def setGossipFile(self, learnfile, sb = "__main"):
		self.gossip_file = learnfile
		self.brains[sb].setGossipFile(learnfile)
	def setVocalize(self, value):
		self.vocalize = value
	def setShowSrais(self, value, sb = "__main"):
		self.showSrais = value
		self.brains[sb].showSrais = value
	def setShowMatches(self, value, sb = "__main"):
		self.showMatches = value
		self.brains[sb].showMatches = value
	def new_aiml(self, aimltext, vocalize = False, socket = None):
		self.aiml_queue.append(aimltext)
		self.aiml_vocalize_queue.append(vocalize)
		self.aiml_socket_queue.append(socket)
		## FIXME: Target: Version 1.0
		##        This should really not do this.  Same as
		##        new_async_response()
		self.process_aiml_queue()
	def create_temp_aiml_file(self, new_aiml_template_text):
		temp_aiml_filename = "/tmp/tempfile.aiml"
		try:
			fp = open(temp_aiml_filename, "w")
			fp.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
			fp.write("<aiml version=\"1.0\">\n")
			## <meta name="author" content="hlpc"/>
			## <meta name="language" content="en"/>
			fp.write("<category>\n")
			fp.write("<pattern>SECRETTASKTAG</pattern>\n")
			fp.write("<template>\n")
			fp.write(re.sub("&","&amp;",new_aiml_template_text))
			fp.write("</template>\n")
			fp.write("</category>\n")
			fp.write("</aiml>\n")
			fp.close()
		except:
			print "Error creating temporary aiml file: "+ temp_aiml_filename
			return ""
		return temp_aiml_filename
	def process_aiml_queue(self):
		sb = "__main"
		while len(self.aiml_queue) > 0:
			new_aiml_text = self.aiml_queue.pop(0)
			new_aiml_vocalize = self.aiml_vocalize_queue.pop(0)
			new_aiml_socket = self.aiml_socket_queue.pop(0)
			temp_aiml_file = self.create_temp_aiml_file(new_aiml_text)
			if temp_aiml_file == "":
				continue
			retval = self.learnUnderscoresOnlyFromFile(temp_aiml_file, False, sb = sb, verbose = False)
			os.unlink(temp_aiml_file)
			resp = self.process_input("secrettasktag", sb = sb, socket = new_aiml_socket)
			self.temp_vocalize = new_aiml_vocalize
			self.process_output(resp, sb = sb, socket = new_aiml_socket)
	def new_async_response(self, text, vocalize = False, socket = None):
		self.async_response_queue.append(text)
		self.async_vocalize_queue.append(vocalize)
		self.async_socket_queue.append(socket)
		## FIXME: Target: Version 1.0
		##        This should really not do this.  The run()
		##        method of this thread should handle printing
		##        but because it's blocking for now on user-input,
		##        this workaround will get the output printed.
		##        Remove when run() is non-blocking.
		self.print_async_response_queue()
	def print_async_response_queue(self):
		## FIXME: These responses might need to be vocalized
		##        or temp_vocalized (like EXEC_TASK)
		while len(self.async_response_queue) > 0:
			temp_vocalize = self.async_vocalize_queue.pop(0)
			temp_socket = self.async_socket_queue.pop(0)
			output = self.async_response_queue.pop(0)
	def saveLastTimeThisName(self, name, force = False):
		self.brains["__main"].saveLastTimeThisName(name, force)
	def getLastTimeThisName(self, name):
		return self.brains["__main"].getLastTimeThisName(name)
	def getPreviousTimeThisName(self, name):
		return self.brains["__main"].getPreviousTimeThisName(name)
		
def parse_args(args):
	"""Parse arguments"""
	global live_aiml_filename, live_gossip_filename
	usage = """check.py"""
	parser = OptionParser(version = "check.py 0.1", usage=usage)
    	# query options
    	parser.add_option("-v", "--verbose", default=False,
			action="store_true", help="Verbose")
    	parser.add_option("-l", "--filelist", default=None,
                      	help="file containing list of AIML files to check")
    	(opts, argsleft) = parser.parse_args(args)

	return opts

def main(args):
	"""main"""

	opts = parse_args(args)
	# The Kernel object is the public interface to
	# the AIML interpreter.
	k = aiml.Kernel()
	k.verbose(False)

	iThread = interactiveThread(k)
	iThread.start()
	# Hold off starting the socket thread until after we're done 
	# parsing our arguments in case we get a new port assignment

	checkfiles = []
	if opts.filelist:
		if os.path.exists(opts.filelist):
		    try:
			fp = open(opts.filelist, "r")
		    except:
			print "Error opening file %s for reading." % (opts.filelist)
			sys.exit()
		    l = fp.readline()
		    while l != "":
			checkfiles.append(l.strip())
		        l = fp.readline()
		    fp.close()
		else:
		    print "File "+ opts.filelist +" does not exist."
		    sys.exit()

	print "Using HLPC trees, producing responses different than standard AIML processing."
	k.setUseHLPCTrees(True)
	iThread.setUseHLPCTrees(True)

	for checkfile in checkfiles:
	    if os.path.exists(checkfile):
		learnstarttime = time.time()
		iThread.learnSkippingConflicts(checkfile, False)
		#print "Learned file: "+ checkfile +" (%.2f seconds)" % (time.time() - learnstarttime)
	    else:
		print "File "+ checkfile +" does not exist."

	iThread.startProcessingInput()

	if len(checkfiles) > 0:
		iThread.process_input_and_output("quit")

	# Wait for interactive thread to finish
	while iThread.isAlive():
		time.sleep(0.1)
		nowtime = time.time()
	
	print "Done."

if __name__ == "__main__":
	main(sys.argv[1:])
