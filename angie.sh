#!/bin/bash
###################################################################
# Copyright (C) BILA
###################################################################

if [ "X$1" = "X--help" -o "X$1" = "X-h" ]; then
	echo "Usage: angie.sh [ --brain BrainFile ] [ ... angie.py arguments ... ]"
	echo "If you'd like to load a saved brain, use the \"--brain BrainFile\" argument."
	echo "Otherwise invoke with zero or more angie.py arguments."
	echo "Supplies angie.py with the following arguments by default:"
	echo "  -g - Enable live gossip file"
	echo "  -t - Enable live template file"
	echo "  -a - Enable the use of ANGie's alternate tree searching algorithm"
	exit
fi

cd ~/angie-0.5.0
if [ "X$1" = "X--brain" -a "X$2" != "X" ]; then
	./angie.py -g -t -a $@
else
	./angie.py -g -t -a -l aiml/std-angie.xml --sub=wise:aiml/std-wise.xml -m 11 $@
fi

